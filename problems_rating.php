<?php

require('include/db.php');
require('include/function.php');

if(!$_SESSION['userID']){
    header('location: login.php');
    exit();
}

$sql = 'SELECT * FROM category INNER JOIN rel_user_category ON rel_user_category.cat_id = category.id WHERE step = 2 AND user_id = :id';
$stmt = $db->prepare($sql);
$stmt->bindParam(':id', $_SESSION['userID']);
$stmt->execute();
$row = $stmt->fetch(PDO::FETCH_ASSOC);

$catid = $row['id'];
$catname = $row['name'];

require('config.php');

$_SESSION['pool'] = getPool(get_current_user_id(), $db);
get_header();
?>
<div class="container emas rating" id="intro">
    <div class="row">
        <div class="menu-wrapper">
          <div class="menu-wrapper-2">
            <?php include('include/title.php'); ?>
          </div>
          <div class="menu-wrapper-3">

          </div>
        </div>
    </div>
    <div class="stage-wrapper">
    <?php include('include/score-wrapper.php'); ?>
    <div class="row">
        <div class="col-md-6">
            <a href="solutions_list.php"><img src="images/emas/stage-2-progress.png"></a>
        </div>
      </div>
      <div class="row">
          <div class="col-md-12 wrapper-category">
            <div class="category-name category-name-<?php echo $row['id']; ?>"><?php echo $row['name']; ?></div>
            <div class="category-desc category-desc-<?php echo $row['id']; ?>"><?php echo $row['description']; ?></div>
          </div>
      </div>
      <div class="row problems-wrapper">
          <div class="col-md-12" style="padding: 0;">
            <div><h3 class="problems-list-title">Problems to validate</h3></div>
            <div id="problems-list"></div>
            <div class="problems-wrapper">
                <div><h3 class="subtitle light-black problems-list-accepted-title"></h3></div>
                <div id="problems-list-accepted"></div>
            </div>
            <div class="problems-wrapper">
                <div><h3 class="subtitle light-black problems-list-rejected-title"></h3></div>
                <div id="problems-list-rejected"></div>
            </div>
        </div>
	</div>
    <div class="row">
          <div class="col-md-12 text-center">
            <a href="solutions_list.php" class="next-stage">Go to next stage</a>
          </div>
        </div>
</div>
<div class="single-problem rating template">
    <div class="button-solution">
        <a href="#" class="accept-problem" data-id="0">Approve</a>
        <a href="#" class="reject-problem" data-id="0">Reject</a>
    </div>
    <div class="title">
        <div class="title-int"></div>
    </div>
    <div class="heading"></div>
    <div class="problem-text"></div>
</div>
</div>
<script src="js/jquery-3.6.0.min.js"></script>
<script src="js/form.js"></script>
<script src="js/main.js"></script>
<script>
var id = <?php echo $_SESSION['userID'];?>;

const configProblem = <?php print json_encode($configProblem); ?>;
const problemForm = new FormBuilder(configProblem, '#target', 'Add this problem');
problemForm.createTemplate();

getProblemsRating(<?php echo $row['id']; ?>);
getProblemsAccepted(<?php echo $row['id']; ?>);
getProblemsRejected(<?php echo $row['id']; ?>);
/****************************/
$('.rating').on('click', '.accept-problem', function(e){
    e.preventDefault();
    const $this = $(this);
    const id = $(this).data('id');
    const data ='id='+id+'&problem=1';
    $.ajax({
        url: 'register_rating_problem.php?'+data,
        method: 'GET',
        success: function(str){
            getProblemsRating(<?php echo $row['id']; ?>);
            getProblemsAccepted(<?php echo $row['id']; ?>);
            getProblemsRejected(<?php echo $row['id']; ?>);
            updatePointsNew();
        }
    });
});
$('.rating').on('click', '.reject-problem', function(e){
    e.preventDefault();
    const $this = $(this);
    const id = $(this).data('id');
    const data ='id='+id+'&problem=0';
    $.ajax({
        url: 'register_rating_problem.php?'+data,
        method: 'GET',
        success: function(str){
            getProblemsRating(<?php echo $row['id']; ?>);
            getProblemsAccepted(<?php echo $row['id']; ?>);
            getProblemsRejected(<?php echo $row['id']; ?>);
            updatePointsNew();
        }
    });
});
$('.rating').on('click', '.shortlist-problem', function(e){
    e.preventDefault();
    const id = $(this).data('id');
    $(this).toggleClass('active');
    if($('.active').length > <?php echo $maxProblems; ?>){
        $(this).toggleClass('active');
        alert('Max <?php echo $maxProblems; ?> problems shortlisted');
    } else{
        if( $(this).hasClass('active') ) {
            s = 3;
            $(this).find('img').attr('src', 'images/emas/shortlist-ok.png');
            $(this).find('span').text('Shortlisted');
        }
        else{
            s = 2;
            $(this).find('img').attr('src', 'images/emas/shortlist-ko.png');
            $(this).find('span').text('Shortlist');
        }
        const data ='id='+id+'&problem=' + s;
        $.ajax({
            url: 'register_rating_problem.php?'+data,
            method: 'GET',
            success: function(str){

            }
        });
    }
});
/**/
let messages = ['scorer-message-1.png', 'scorer-message-2.png', 'scorer-message-3.png', 'scorer-message-4.png', 'scorer-message-5.png', 'scorer-message-6.png'];
var item = messages[Math.floor(Math.random()*messages.length)];
function updatePointsNew(){
        console.log('updatePoints');
        $.ajax({
            url: 'scorer.php?id=' + id,
            type: 'GET',
            dataType: 'json',
            success: function(data){
                }
        });
    }
</script>
<?php
get_footer();

