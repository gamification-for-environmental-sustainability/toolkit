<?php

require('include/db.php');
require('include/function.php');

$contenuto='';
$action='';if(isset($_REQUEST['action'])){$action=$_REQUEST['action'];}
switch($action){
    case 'register':
        $contenuto=register();
        break;
}

function register() {
    global $db;
    $token = filter_var($_POST['CSRFToken'], FILTER_SANITIZE_STRING);
    if (!$token || $token !== $_SESSION['token']) {
        // return 405 http status code
        header($_SERVER['SERVER_PROTOCOL'] . ' 405 Method Not Allowed');
        exit;
    } else {
        $user=filter_var(trim($_POST['username']), FILTER_SANITIZE_STRING);
        $pwd=filter_var(trim($_POST['password']), FILTER_SANITIZE_STRING);
        $pwd=password_hash($pwd, PASSWORD_BCRYPT);
        $sql='INSERT INTO users (username, password) VALUES (:user, :pwd) ';
        $stmt=$db->prepare($sql);
        $stmt->bindParam(':user', $user);
        $stmt->bindParam(':pwd', $pwd);
        $stmt->execute();
    }
}

if(!isset($_SESSION['token'])) {
    $_SESSION['token'] = md5(uniqid(mt_rand(), true));
}
get_header();
?>
<div class="container emas login" id="intro">
    <div class="row">
        <div class="col-md-12">
            <h1 class="welcome-title">Gamification<br>for environmental<br>sustainability</h1>
            <h4 class="welcome-text">A JRC Living Lab pilot project testing game-based<br>approaches to citizen engagement</h4>
        </div>
    </div>
    <br><br><br><br><br><br>
    <div class="row form-login">
        <div class="col-md-4"></div>
        <div class="col-md-4">
            <h1 class="form-login-sign-in">Sign-in</h1>
            <h4 class="form-login-sign-in-text">Load your game and play to win points.</h4>
            <p><?php echo $contenuto; ?></p>
            <form name="loginform" id="loginform" action="?action=register" method="post">
                <input type="hidden" name="CSRFToken" value="<?php echo $_SESSION['token']; ?>">
                <p class="login-username">
                    <label for="user">Nome utente o indirizzo email</label>
                    <input type="text" name="username" id="user" class="input" value="" size="20">
                </p>
                <p class="login-password">
                    <label for="pass">Password</label>
                    <input type="password" name="password" id="pass" class="input" value="" size="20">
                </p>
                <p class="login-submit">
                    <input type="submit" name="wp-submit" id="wp-submit" class="button button-primary" value="Sign-in">
                </p>
            </form>
        </div>
        <div class="col-md-4"></div>
    </div>
</div>
<script src="js/jquery-3.6.0.min.js"></script>
<script>
$('#intro').css({'background-image': 'url(images/emas/bg8.png)', 'bacground-repeat': 'no-repeat', 'background-size':'cover', 'background-position': '0'});
</script>
<?php
get_footer();

