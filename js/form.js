class FormBuilder {
    constructor(fieldList, target, submitText) {
        this.fieldList = fieldList;
        this.target = target;
        this.submitText = submitText;
    }
    createForm() {
        $.each(this.fieldList, (i, el) => {
            const field = 'field' + el.field;
            $(this.target).append('<div id="' + field + '" class="wrapper-problem-input"></div>');
            $('#' + field)
                .append('<div class="wrapper-text"></div>')
                .append('<div class="wrapper-input"></div>');
            $('#' + field + ' .wrapper-text ')
                .append('<p class="text1">' + el.text1 + '</p>')
                .append('<p class="text2">' + el.text2 + '</p>');
            let inputName;
            switch(el.type){
                case 'textarea':
                    inputName = el.db;
                    $('#' + field + ' .wrapper-input ')
                        .append('<textarea id="' + inputName + '" name="' + inputName + '" placeholder="' + el.placeholder + '"></textarea>')
                        .append('<p id="msg' + inputName + '"><span>250</span> character left</p>');
                    this.checkChr(el.max, inputName, 'msg' + inputName);
                    break;
                case 'hidden':
                    inputName = el.db;
                    $('#' + field + ' .wrapper-input ')
                        .append('<input type="' + el.type + '" id="' + inputName + '" name="' + inputName + '" placeholder="' + el.placeholder + '" value="' + el.value + '">')
                    break;
            }
        });
        $(this.target).after('<button type="submit">' + this.submitText + '</button>');
    }
    createTemplate(){
        $.each(this.fieldList, (i, el) => {
            let inputName;
            switch(el.type){
                case 'textarea':
                    inputName = el.type + '_' + el.field;
                    $('.single-problem.template .heading')
                        .append('<p class="' + inputName +'"></p>');
                    $('.single-problem.template .problem-text')
                        .append('<p class="' + inputName + '"></p>');
                    break;
            }
        });
    }
    createTemplate2(){
        $.each(this.fieldList, (i, el) => {
            let inputName;
            switch(el.type){
                case 'textarea':
                    inputName = el.type + '_' + el.field;
                    $('.single-problem.template-2 .heading')
                        .append('<p class="' + inputName +'"></p>');
                    $('.single-problem.template-2 .problem-text')
                        .append('<p class="' + inputName + '"></p>');
                    break;
            }
        });
    }
    checkChr(num, ctrl, out){
        let t2 = 0; 
        if($('#'+ctrl).length > 0){
            t2 = $('#'+ctrl).val().length;
        }
        $('#'+out).html('<span>' + (num - t2) + '<span> character left');;
        $('#'+ctrl).on('keyup', function(){
            let k = $(this).val();
            let t = $(this).val().length;
            $('#'+out).html('<span>' + (num - t) +'<span> character left');;
            if(t > num){
                $(this).val(k.substr(0,num));
                $('#' + out + ' span').html('<span>0<span> character left');
            }
        });
    }
}