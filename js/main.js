function updatePoints(){
    $.ajax({
        url: 'scorer.php?id=' + id,
        type: 'GET',
        dataType: 'json',
        success: function(data){
            $('.point').text(data.points);
            $('#you_wrapper').rotate(data.gradi);

            var img1 = document.createElement('img');
            img1.src = "images/emas/scorer-happy-animation.gif?p" + new Date().getTime();

            var img2 = document.createElement('img');
            img2.src = "images/emas/scorer-neutral.png?p" + new Date().getTime();

            if($('.stage-wrapper').hasClass('animated')){
                $(img1).on('load',function(){
                    $('#wrapper_circonferenza').css('background-image', 'url(' + img1.src + ')');
                });
                $('#circonferenza').append('<img src="images/emas/'+item+'" class="messages" id="msg1">');
                $('#msg1').delay(2000).fadeOut(6000);
                } else{
                    $(img2).on('load',function(){
                        $('#wrapper_circonferenza').css('background-image', 'url(' + img2.src + ')');
                    });
                }
            }
    });
}
function getProblems(id){ /**new */
    $.ajax({
        url: 'problems_list_json.php?id='+id,
        type: 'GET',
        dataType: 'json',
        success:function(data){
            $('#problems-list').empty();
            if( data.length == 0){ $('.problems-list-title').text('No identified problems yet'); }
            else{ $('.problems-list-title').text('Identified problems'); }
            $.each(data, function(i, el){
                const strHtml = $('.template').clone().removeClass('template');
                const row = combine(strHtml, el);
                $('#problems-list').append(row);
            });
            /***[TODO *** problem two lines fix]*/
            const list = $('.single-problem');
            let bh = 0;
            $.each(list, (i, el) => {
                bh = $(el).find('.problem-text .textarea_1').height();
                //console.log(bh);
                if(bh > 30) {
                    $(el).find('.heading .textarea_1').height(bh - 6);
                }
                bh = $(el).find('.problem-text .textarea_2').height();
                //console.log(bh);
                if(bh > 30) {
                    $(el).find('.heading .textarea_2').height(bh - 6);
                }
                bh = $(el).find('.problem-text .textarea_3').height();
                //console.log(bh);
                if(bh > 30) {
                    $(el).find('.heading .textarea_3').height(bh - 6);
                }
                bh = $(el).find('.problem-text .textarea_4').height();
                //console.log(bh);
                if(bh > 30) {
                    $(el).find('.heading .textarea_4').height(bh - 6);
                }
            });
        }
    });
}
function getProblemsRating(id){
    $.ajax({
        url: 'problems_list_json.php?id='+id,
        type: 'GET',
        dataType: 'json',
        success:function(data){
            if( data.length == 0){
                if(countProblemsAccepted()==0 && countProblemsRejected==0){
                    $('.problems-list-title').text('No identified problems yet');
                    $('.subtitle-proposed').text('');
                }
                else{
                    $('.problems-list-title').hide();
                }

            }
            else{ $('.problems-list-title').text('Problems to validate'); }
            $('#problems-list').empty();
            $.each(data, function(i, el){
                const strHtml = $('.template').clone().removeClass('template');
                const row = combine(strHtml, el);
                $('#problems-list').append(row);
            });
            /***[TODO *** problem two lines fix]*/
            const list = $('.single-problem');
            let bh = 0;
            $.each(list, (i, el) => {
                bh = $(el).find('.problem-text .textarea_1').height();
                console.log(bh);
                if(bh > 30) {
                    $(el).find('.heading .textarea_1').height(bh - 6);
                }
                bh = $(el).find('.problem-text .textarea_2').height();
                console.log(bh);
                if(bh > 30) {
                    $(el).find('.heading .textarea_2').height(bh - 6);
                }
                bh = $(el).find('.problem-text .textarea_3').height();
                console.log(bh);
                if(bh > 30) {
                    $(el).find('.heading .textarea_3').height(bh - 6);
                }
                bh = $(el).find('.problem-text .textarea_4').height();
                console.log(bh);
                if(bh > 30) {
                    $(el).find('.heading .textarea_4').height(bh - 6);
                }
            });
        }
    });
}
function getProblemsAccepted(id){
    $.ajax({
        url: 'problems_list_json.php?action=accept&id='+id,
        type: 'GET',
        dataType: 'json',
        success:function(data){
            $('#problems-list-accepted').empty();
            if(data.length == 0){
                $('.problems-list-accepted-title').text('No approved problems yet').hide();
            }
            else{
                $('.problems-list-accepted-title').text('Approved problems');
            }
            $.each(data, function(i, el){
                if( el.rating_shortlist == 1){
                    c = 'active';
                    img = 'shortlist-ok.png';
                    txt ='Shortlisted';
                }
                else{
                    c = '';
                    img = 'shortlist-ko.png';
                    txt = 'Shortlist';
                }
                const strHtml = $('.template').clone().removeClass('template');
                const row = combine(strHtml, el);
                $('#problems-list-accepted').append(row);
                $('#problems-list-accepted .accept-problem')
                    .removeClass('accept-problem')
                    .addClass('shortlist-problem ' + c)
                    .html('<img src="images/emas/' + img + '" width="30">&nbsp;<span>' + txt + '</span>');
            });
            /***[TODO *** problem two lines fix]*/
            const list = $('.single-problem');
            let bh = 0;
            $.each(list, (i, el) => {
                bh = $(el).find('.problem-text .textarea_1').height();
                console.log(bh);
                if(bh > 30) {
                    $(el).find('.heading .textarea_1').height(bh - 6);
                }
                bh = $(el).find('.problem-text .textarea_2').height();
                console.log(bh);
                if(bh > 30) {
                    $(el).find('.heading .textarea_2').height(bh - 6);
                }
                bh = $(el).find('.problem-text .textarea_3').height();
                console.log(bh);
                if(bh > 30) {
                    $(el).find('.heading .textarea_3').height(bh - 6);
                }
                bh = $(el).find('.problem-text .textarea_4').height();
                console.log(bh);
                if(bh > 30) {
                    $(el).find('.heading .textarea_4').height(bh - 6);
                }
            });
        }
    });
}
function getProblemsRejected(id){
    $.ajax({
        url: 'problems_list_json.php?action=rejected&id='+id,
        type: 'GET',
        dataType: 'json',
        success:function(data){
            $('#problems-list-rejected').empty();
            if(data.length == 0){
                $('.problems-list-rejected-title').text('No rejected problems yet').hide();
            }
            else{
                $('.problems-list-rejected-title').text('Rejected problems');
            }
            $.each(data, function(i, el){
                const strHtml = $('.template').clone().removeClass('template');
                const row = combine(strHtml, el);
                $('#problems-list-rejected').append(row);
                $('#problems-list-rejected .reject-problem').remove();
            });
            /***[TODO *** problem two lines fix]*/
            const list = $('.single-problem');
            let bh = 0;
            $.each(list, (i, el) => {
                bh = $(el).find('.problem-text .textarea_1').height();
                console.log(bh);
                if(bh > 30) {
                    $(el).find('.heading .textarea_1').height(bh - 6);
                }
                bh = $(el).find('.problem-text .textarea_2').height();
                console.log(bh);
                if(bh > 30) {
                    $(el).find('.heading .textarea_2').height(bh - 6);
                }
                bh = $(el).find('.problem-text .textarea_3').height();
                console.log(bh);
                if(bh > 30) {
                    $(el).find('.heading .textarea_3').height(bh - 6);
                }
                bh = $(el).find('.problem-text .textarea_4').height();
                console.log(bh);
                if(bh > 30) {
                    $(el).find('.heading .textarea_4').height(bh - 6);
                }
            });
        }
    });
}
function countProblemsAccepted(id){
    $.ajax({
        url: 'problems_list_json.php?action=accept&id='+id,
        type: 'GET',
        dataType: 'json',
        async: false,
        success:function(data){
            return (data.length);
        }
    });
}
function countProblemsRejected(id){
    $.ajax({
        url: 'problems_list_json.php?action=rejected&id='+id,
        type: 'GET',
        dataType: 'json',
        async: false,
        success:function(data){
            return (data.length);
        }
    });
}
function getProblemsShortlisted(id){
    $.ajax({
        url: 'problems_list_json.php?action=shortlisted&id='+id,
        type: 'GET',
        dataType: 'json',
        success:function(data){
            $('#problems-list-shortlisted').empty();
            if(data.length == 0){
                $('#problems-list').append('<div class="single-problem"><h3 class="problems-list-title" style="text-align: center;">No problems shortlisted for this topic</h3></div>');
            }
            else{
                $.each(data, function(i, el){
                    const strHtml = $('.template').clone().removeClass('template');
                    const row = combine(strHtml, el);
                    $('#problems-list').append(row);
                });
                /***[TODO *** problem two lines fix]*/
                const list = $('.single-problem');
                let bh = 0;
                $.each(list, (i, el) => {
                    bh = $(el).find('.problem-text .textarea_1').height();
                    console.log(bh);
                    if(bh > 30) {
                        $(el).find('.heading .textarea_1').height(bh - 6);
                    }
                    bh = $(el).find('.problem-text .textarea_2').height();
                    console.log(bh);
                    if(bh > 30) {
                        $(el).find('.heading .textarea_2').height(bh - 6);
                    }
                    bh = $(el).find('.problem-text .textarea_3').height();
                    console.log(bh);
                    if(bh > 30) {
                        $(el).find('.heading .textarea_3').height(bh - 6);
                    }
                    bh = $(el).find('.problem-text .textarea_4').height();
                    console.log(bh);
                    if(bh > 30) {
                        $(el).find('.heading .textarea_4').height(bh - 6);
                    }
                });
            }
        }
    });
}
function getProblemsShortlistedSolutions(id){
    $.ajax({
        url: 'problems_list_json.php?action=shortlisted_solutions&id='+id,
        type: 'GET',
        dataType: 'json',
        success:function(data){
            $('#problems-list-shortlisted').empty();
            if(data.length == 0){
                $('#problems-list').append('<div class="single-problem"><h3 class="problems-list-title" style="text-align: center;">No problems shortlisted for this topic</h3></div>');
            }
            else{
                let strHtml = '';
                let active = 0;
                const tot = data.length;
                $('#tot').html(tot);
                let display = '';
                $.each(data, function(i, el){
                    const strHtml = $('.template').clone().removeClass('template');
                    const row = combine(strHtml, el);
                    if( el.solution.length == 0 ) {
                        solutionText = 'No identified solutions yet';
                        h = 'style=" height: 0; " ';
                    }
                    else {
                        solutionText = 'Proposed solutions';
                        h = '';
                    }
                    $('#problems-list-shortlisted').append(row);
                    $(row).after('<div class="wrapper-solutions wrapper-solutions-' + el.id + '" ' + h + '><span class="solution-title">' + solutionText + '</span></div>');
                    t = '.wrapper-solutions-' + el.id;
                    $.each(el.solution, function(index, element){
                        const strHtml = $('.template-2').clone().removeClass('template-2');
                        const rown = combine(strHtml, element, index + 1);
                        $(t).append(rown);
                    });
                    $('#problems-list-shortlisted').append('<div class="problem-separator-wrapper"><div class="problem-separator"></div></div>');
                });
                $('#problems-list-shortlisted .wrapper-solutions').after('<button type="button" class="add-solution">Add solution</button>');
                $('#problems-list-shortlisted .single-problem').after('<div class="wrapper-form-solution"></div>');  
                
                /***[TODO *** problem two lines fix]*/
                const list = $('.single-problem.single-solution');
                let bh = 0;
                $.each(list, (i, el) => {
                    bh = $(el).find('.problem-text .textarea_1').height();
                    if(bh > 30) {
                        $(el).find('.heading .textarea_1').height(bh - 6);
                    }
                    bh = $(el).find('.problem-text .textarea_2').height();
                    if(bh > 30) {
                        $(el).find('.heading .textarea_2').height(bh - 6);
                    }
                    bh = $(el).find('.problem-text .textarea_3').height();
                    if(bh > 30) {
                        $(el).find('.heading .textarea_3').height(bh - 6);
                    }
                    bh = $(el).find('.problem-text .textarea_4').height();
                    if(bh > 30) {
                        $(el).find('.heading .textarea_4').height(bh - 6);
                    }
                });
                
            }
        }
    });
}
function getSolutions(id){
    $.ajax({
        url: 'solutions_list_json.php?id='+id,
        type: 'GET',
        dataType: 'json',
        success:function(data){
            if( data.length == 0){
                $('.solutions-list-title').text('No identified solutions yet');
                $('.subtitle-proposed').text('');
            }
            else{ $('.solutions-list-title').text('Identified solutions'); }
            $('#solutions-list').empty();
            $.each(data, function(i, el){
                const strHtml = $('.template').clone().removeClass('template');
                const row = combine(strHtml, el);
                $('#solutions-list').append(row);
                $(row).after('<div class="wrapper-solutions wrapper-solutions-' + el.id + '"></div>');
                t = '.wrapper-solutions-' + el.id;
                if(el.solutionn && el.solutionn.length > 0){
                    $(t).append('<p class="solution-rating-title">Approve or Reject the solution</p>');
                }
                $.each(el.solutionn, function(index, element){
                    const strHtml = $('.template-2').clone().removeClass('template-2').addClass('solution-n');
                    const rown = combine(strHtml, element, index + 1);
                    $(t).append(rown);
                });
                if(el.solutiona && el.solutiona.length > 0){
                    $(t).append('<p class="solution-rating-title">Approved solutions</p>');
                }
                $.each(el.solutiona, function(index, element){
                    if( element.rating_shortlist == 1){
                        c = 'active';
                        img = 'shortlist-ok.png';
                        txt = 'Shortlisted';
                    }
                    else{
                        c = '';
                        img = 'shortlist-ko.png';
                        txt = 'Shortlist';
                    }
                    const strHtml = $('.template-2').clone().removeClass('template-2').addClass('solution-a');
                    const rown = combine(strHtml, element, index + 1);
                    $(rown).find('.accept-problem')
                        .removeClass('accept-problem')
                        .addClass('shortlist-problem ' + c)
                        .html('<img src="images/emas/' + img + '" width="30">&nbsp;<span>' + txt + '</span>');
                    $(t).append(rown);
                });
                if(el.solutionr && el.solutionr.length > 0){
                    $(t).append('<p class="solution-rating-title">Rejected solutions</p>');
                }
                $.each(el.solutionr, function(index, element){
                    const strHtml = $('.template-2').clone().removeClass('template-2').addClass('solution-r');
                    const rown = combine(strHtml, element, index + 1);
                    $(rown).find('.reject-problem').remove();
                    $(t).append(rown);
                });
                
                /***[TODO *** problem two lines fix]*/
                const list = $('.single-problem.single-solution');
                let bh = 0;
                $.each(list, (i, el) => {
                    bh = $(el).find('.problem-text .textarea_1').height();
                    console.log(bh);
                    if(bh > 30) {
                        $(el).find('.heading .textarea_1').height(bh);
                    }
                    bh = $(el).find('.problem-text .textarea_2').height();
                    console.log(bh);
                    if(bh > 30) {
                        $(el).find('.heading .textarea_2').height(bh);
                    }
                    bh = $(el).find('.problem-text .textarea_3').height();
                    console.log(bh);
                    if(bh > 30) {
                        $(el).find('.heading .textarea_3').height(bh);
                    }
                    bh = $(el).find('.problem-text .textarea_4').height();
                    console.log(bh);
                    if(bh > 30) {
                        $(el).find('.heading .textarea_4').height(bh);
                    }
                });
                $('#solutions-list').append('<div class="problem-separator-wrapper"><div class="problem-separator-2"></div></div>'); 
            });
        }
    });
}
/*
function getSolutionsAccepted(id){
    $.ajax({
        url: 'solutions_list_json.php?action=accept&id='+id,
        type: 'GET',
        dataType: 'json',
        success:function(data){
            $('#solutions-list-accepted').empty();
            if(data.length == 0){
                $('.solutions-list-accepted-title').text('No approved solutions yet');
            }
            else{
                $('.solutions-list-accepted-title').text('Approved solutions');
            }
            let strHtml = '';
            $.each(data, function(i, el){
                if(el.description_1 == null) { el.description_1 = ''; }
                if(el.description_2 == null) { el.description_2 = ''; }
                if(el.description_3 == null) { el.description_3 = ''; }
                strHtml += '<div class="single-problem">' +
                    '<div class="problem-title">' + el.title + '</div>' +
                    '<div class="problem-text">' + el.description_1.stripSlashes() + '</div>' +
                    '<div class="problem-text">' + el.description_2.stripSlashes() + '</div>' +
                    '<div class="problem-text">' + el.description_3.stripSlashes() + '</div>' +
                    '</div>';
                $.each(el.solution, function(index, element){
                    if(element.description_1 == null) { element.description_1 = ''; }
                    if(element.description_2 == null) { element.description_2 = ''; }
                    if(element.description_3 == null) { element.description_3 = ''; }
                    if( element.rating_shortlist == 1){
                        c = 'active';
                        img = 'shortlist-ok.png';
                    }
                    else{
                        c = '';
                        img = 'shortlist-ko.png';
                    }
                    strHtml += '<div class="single-solution" data-id="' + element.id + '">' +
                        '<div class="solution">' +
                        '<div class="solution-counter">'+ (index + 1) +'.</div>' +
                        '<div class="wrapper-solution-text">' +
                        '<div class="solution-text">' + element.description_1.stripSlashes() + '</div>' +
                        '<div class="solution-text">' + element.description_2.stripSlashes() + '</div>' +
                        '<div class="solution-text">' + element.description_3.stripSlashes() + '</div>' +
                        '</div>' +
                        '<div class="shortlist"><a href="#" class="shortlist-solution ' + c + '" data-id="' + el.id + '"><img src="images/emas/' + img + '" width="89"></a></div>' +
                        '</div>' +
                        '<div class="button-solution">' +
                        '<a href="#" class="reject-solution" data-id="' + element.id + '">Reject</a>' +
                        '<a href="#" class="accept-solution" data-id="' + element.id + '">Approve</a>' +
                        '</div>' +
                        '</div>'
                });
            });
            $('#solutions-list-accepted').append(strHtml);
        }
    });
}
*/
/*
function getSolutionsRejected(id){
    $.ajax({
        url: 'solutions_list_json.php?action=rejected&id='+id,
        type: 'GET',
        dataType: 'json',
        success:function(data){
            $('#solutions-list-rejected').empty();
            if(data.length == 0){
                $('.solutions-list-rejected-title').text('No rejected solutions yet');
            }
            else{
                $('.solutions-list-rejected-title').text('Rejected solutions');
            }
            let strHtml = '';
            $.each(data, function(i, el){
                if(el.description_1 == null) { el.description_1 = ''; }
                if(el.description_2 == null) { el.description_2 = ''; }
                if(el.description_3 == null) { el.description_3 = ''; }
                strHtml += '<div class="single-problem">' +
                    '<div class="problem-title">' + el.title + '</div>' +
                    '<div class="problem-text">' + el.description_1.stripSlashes() + '</div>' +
                    '<div class="problem-text">' + el.description_2.stripSlashes() + '</div>' +
                    '<div class="problem-text">' + el.description_3.stripSlashes() + '</div>' +
                    '</div>';
                $.each(el.solution, function(index, element){
                    if(element.description_1 == null) { element.description_1 = ''; }
                    if(element.description_2 == null) { element.description_2 = ''; }
                    if(element.description_3 == null) { element.description_3 = ''; }
                    if( element.rating_shortlist == 1){
                        c = 'active';
                        img = 'shortlist-ok.png';
                    }
                    else{
                        c = '';
                        img = 'shortlist-ko.png';
                    }
                    strHtml += '<div class="single-solution" data-id="' + element.id + '">' +
                        '<div class="solution">' +
                        '<div class="solution-counter">'+ (index + 1) +'.</div>' +
                        '<div>' +
                        '<div class="solution-text">' + element.description_1.stripSlashes() + '</div>' +
                        '<div class="solution-text">' + element.description_2.stripSlashes() + '</div>' +
                        '<div class="solution-text">' + element.description_3.stripSlashes() + '</div>' +
                        '</div>' +
                        '</div>' +
                        '<div class="button-solution">' +
                        '<a href="#" class="accept-solution" data-id="' + element.id + '">Approve</a>' +
                        '</div>' +
                        '</div>'
                });
            });
            $('#solutions-list-rejected').append(strHtml);
        }
    });
}
*/
function getSolutionsShortlisted(id){
    $.ajax({
        url: 'solutions_list_json.php?action=shortlisted&id='+id,
        type: 'GET',
        dataType: 'json',
        success:function(data){
            $('#solutions-list').empty();
            if(data.length == 0){
                $('#solutions-list').append('<div class="single-solution"><h3 class="problems-list-title" style="text-align: center;">No solutions shortlisted for this topic</h3></div>');
                $('.submit-idea-button').text('Next step');
            }
            else{
                $('.submit-idea-button').text('Confirm your investment');
                const arrayLetters = ['A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z'];
                $.each(data, function(i, el){
                    el.title = arrayLetters[i];
                    const strHtml = $('.template').clone().removeClass('template').addClass('problem-sl');
                    const row = combine(strHtml, el);
                    $('#solutions-list').append(row);
                    $(row).after('<div class="wrapper-solutions wrapper-solutions-' + el.id + '"></div>');
                    t = '.wrapper-solutions-' + el.id;
                    if(el.solution.length > 0){
                        //$(t).append('<p class="solution-rating-title">Assign your tokens</p>');
                        $(t).append('<p class="solution-rating-title"></p>');
                    }
                    $.each(el.solution, function(index, element){
                        const strHtml = $('.template-2').clone().removeClass('template-2').addClass('solution-sl');
                        const rown = combine(strHtml, element, index + 1);
                        $(rown).find('.value').attr('id', 'solution' + element.id).attr('data-id', element.id);
                        $(rown).find('.minus').attr('data-id', element.id);
                        $(rown).find('.plus').attr('data-id', element.id);
                        $(t).append(rown);
                    });
                    $('#solutions-list').append('<div class="problem-separator-wrapper"><div class="problem-separator-2"></div></div>'); 
                });
                /***[TODO *** problem two lines fix]*/
                const list0 = $('.problem-sl');
                let bh0 = 0;
                $.each(list0, (i, el) => {
                    bh0 = $(el).find('.problem-text .textarea_1').height();
                    console.log(bh0);
                    if(bh0 > 30) {
                        $(el).find('.problem-text .textarea_1').css('margin-bottom', 0);
                    }
                    else{
                        $(el).find('.problem-text .textarea_1').css({
                            'padding-bottom': 0,
                            'padding-top': '6px'
                        });
                        $(el).find('.heading .textarea_1').css({
                            'padding-top': '6px'
                        });
                    }
                });
                const list = $('.single-problem.single-solution');
                let bh = 0;
                $.each(list, (i, el) => {
                    bh = $(el).find('.problem-text .textarea_1').height();
                    if(bh > 30) {
                        $(el).find('.heading .textarea_1').height(bh);
                    }
                    bh = $(el).find('.problem-text .textarea_2').height();
                    if(bh > 30) {
                        $(el).find('.heading .textarea_2').height(bh);
                    }
                    bh = $(el).find('.problem-text .textarea_3').height();
                    if(bh > 30) {
                        $(el).find('.heading .textarea_3').height(bh);
                    }
                    bh = $(el).find('.problem-text .textarea_4').height();
                    if(bh > 30) {
                        $(el).find('.heading .textarea_4').height(bh);
                    }
                });
            }
        }
    });
}

$(document).ready(function(){
    jQuery.fn.rotate = function(degrees) {
        $(this).css({'transform' : 'rotate('+ degrees +'deg)'});
        return $(this);
    };

    let messages = ['scorer-message-1.png', 'scorer-message-2.png', 'scorer-message-3.png', 'scorer-message-4.png', 'scorer-message-5.png', 'scorer-message-6.png'];
    var item = messages[Math.floor(Math.random()*messages.length)];
    updatePoints();
    /****************************/
});
String.prototype.stripSlashes = function(){
    return this.replace(/\\(.)/mg, "$1");
}
function combine(strHtml, el, index=0){
    if(index == 0){
        title = el.title.stripSlashes();
    }
    else{
        title = index;
    }

    $(strHtml).find('.title').attr('id', el.id);
    $(strHtml).find('.title-int').html(title);
    $(strHtml).find('.accept-problem').attr('data-id', el.id);
    $(strHtml).find('.reject-problem').attr('data-id', el.id);
    $.each(configProblem, (i, element) => {
        switch(element.type){
            case 'textarea':
                inputName = element.type + '_' + element.field;
                $(strHtml).find('.heading .' + inputName).html(element.text1);
                $(strHtml).find('.problem-text .' + inputName).html(el[element.db] + '&nbsp;');
                //$(strHtml).find('.problem-text .' + inputName).html(el[element.db].stripSlashes());
                break;
        }
    });

    return strHtml;
}