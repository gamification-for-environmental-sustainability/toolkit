<?php
/* DB Connection Setup */
$hostname = 'localhost';
$dbname = 'gamification_toolkit';
$user = '';
$pass = '';
/* Topic Setup */
$topicName1 = "Buildings";
$topicDesc1 = "Buildings account for 40% of our carbon footprint – how can we lower the environmental impact of their design and daily management?";
$topicName2 = "Appliances";
$topicDesc2 = "Are our appliances energy efficient enough? How could we help staff use these appliances in a more environmentally responsible way?";
$topicName3 = "Behaviour";
$topicDesc3 = "How can the everyday awareness of staff be raised in order to achieve more energy efficient behaviour?";

/* Max Problems/Solutions Setup */
$maxProblems = 5;
$maxSolutions = 3;
/* Score Setup */
$pointsProblem = 50;
$pointsSolution = 50;

$pointsProblemApproved = 50;
$pointsSolutionApproved = 150;

$pointsProblemShortlisted = 0;
$pointsSolutionShortlisted = 0;

$pointsSolutionRated = 15;

$WinningSolutionPos1 = 200;
$WinningSolutionPos2 = 150;
$WinningSolutionPos3 = 100;
/*Structure Setup */
$configProblem = [
    ["field"=>1, "db"=>"description_1", "text1"=>"WHAT", "text2"=>"problem description",  "type"=> "textarea", "max"=>250, "placeholder"=>"Text goes here in maximum two rows"],
    ["field"=>2, "db"=>"description_2", "text1"=>"WHERE", "text2"=>"field of applicability",  "type"=> "textarea", "max"=>250, "placeholder"=>"Text goes here in maximum two rows"],
    ["field"=>3, "db"=>"description_3", "text1"=>"WHO", "text2"=>"stakeholders involved",  "type"=> "textarea", "max"=>250, "placeholder"=>"Text goes here in maximum two rows"],
    ["field"=>4, "db"=>"description_4", "text1"=>"WHY", "text2"=>"reason behind",  "type"=> "textarea", "max"=>250, "placeholder"=>"Text goes here in maximum two rows"],
    ["field"=>5, "db"=>"cat_id", "text1"=>"", "text2"=>"",  "type"=> "hidden", "max"=>0, "placeholder"=>"", "value"=>$catid],
    ["field"=>6, "db"=>"cat_name", "text1"=>"", "text2"=>"",  "type"=> "hidden", "max"=>0, "placeholder"=>"", "value"=>$catname],
];
$configSolution = [
    ["field"=>1, "db"=>"description_1", "text1"=>"WHAT", "text2"=>"solution description",  "type"=> "textarea", "max"=>250, "placeholder"=>"Text goes here in maximum two rows"],
    ["field"=>2, "db"=>"description_2", "text1"=>"WHERE", "text2"=>"field of applicability",  "type"=> "textarea", "max"=>250, "placeholder"=>"Text goes here in maximum two rows"],
    ["field"=>3, "db"=>"description_3", "text1"=>"WHO", "text2"=>"stakeholders involved",  "type"=> "textarea", "max"=>250, "placeholder"=>"Text goes here in maximum two rows"],
    ["field"=>4, "db"=>"cat_id", "text1"=>"", "text2"=>"",  "type"=> "hidden", "max"=>0, "placeholder"=>"", "value"=>$catid],
    ["field"=>5, "db"=>"cat_name", "text1"=>"", "text2"=>"",  "type"=> "hidden", "max"=>0, "placeholder"=>"", "value"=>$catname],
    ["field"=>6, "db"=>"problem_id", "text1"=>"", "text2"=>"",  "type"=> "hidden", "max"=>0, "placeholder"=>"", "value"=>""],
];