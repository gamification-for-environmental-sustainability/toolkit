Push an existing Git repository with the following command:

```
cd existing_repo
git remote add origin https://code.europa.eu/gamification-for-environmental-sustainability/toolkit.git
git branch -M main
git push -uf origin main
```

## Installation and usage
Please refer to the file "Manual.pdf" under folder DOC.

## Description
Welcome to the source code for replicating your own website version of the "Gamification for Environmental Sustainability", developed by JRC C.3 (Living Lab for testing digital energy solutions)

For questions and comments, please write to: 
JRC-ENERGY-LIVING-LAB@ec.europa.eu
## License
This is an open source software released under EUPL 1.2


