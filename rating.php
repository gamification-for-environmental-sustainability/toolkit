<?php

require('include/db.php');
require('include/function.php');

if(!$_SESSION['userID']){
    header('location: login.php');
    exit();
}

$sql = 'SELECT * FROM category INNER JOIN rel_user_category ON rel_user_category.cat_id = category.id WHERE step = 4 AND user_id = :id';
$stmt = $db->prepare($sql);
$stmt->bindParam(':id', $_SESSION['userID']);
$stmt->execute();
$row = $stmt->fetch(PDO::FETCH_ASSOC);
$_SESSION['pool'] = getPool(get_current_user_id(), $db);

$catid = $row['id'];
$catname = $row['name'];

require('config.php');

get_header();
?>
<div class="container emas rating" id="intro">
    <div class="row">
        <div class="menu-wrapper">
          <div class="menu-wrapper-2">
            <?php include('include/title.php'); ?>
          </div>
          <div class="menu-wrapper-3">

          </div>
        </div>
    </div>
    <div class="stage-wrapper">
    <?php include('include/score-wrapper.php'); ?>
    <div class="row">
        <div class="col-md-6">
            <a href="investor.php"><img src="images/emas/stage-4-progress.png"></a>
        </div>
      </div>
      <div class="row">
          <div class="col-md-12 wrapper-category">
            <div class="category-name category-name-<?php echo $row['id']; ?>"><?php echo $row['name']; ?></div>
            <div class="category-desc category-desc-<?php echo $row['id']; ?>"><?php echo $row['description']; ?></div>
          </div>
      </div>
      <div class="row problems-wrapper">
          <div class="col-md-12" style="padding: 0;">
            <div class="solutions-wrapper">
                <div><h3 class="subtitle light-black subtitle-proposed">Validating + shortlisting solutions</h3></div>
                <div id="solutions-list"></div>
            </div>
        </div>
	</div>
    <div class="row">
          <div class="col-md-12 text-center">
            <a href="investor.php" class="next-stage">Go to next stage</a>
          </div>
        </div>
</div>
<div class="single-problem template">
    <div class="title">
        <div class="title-int"></div>
    </div>
    <div class="heading"></div>
    <div class="problem-text"></div>
</div>

<div class="single-problem single-solution template-2">
    <div class="button-solution">
        <a href="#" class="accept-problem" data-id="0">Approve</a>
        <a href="#" class="reject-problem" data-id="0">Reject</a>
    </div>
    <div class="title">
        <div class="title-int"></div>
    </div>
    <div class="heading"></div>
    <div class="problem-text"></div>
</div>
</div>
<script src="js/jquery-3.6.0.min.js"></script>
<script src="js/form.js"></script>
<script src="js/main.js"></script>
<script>
var id = <?php echo $_SESSION['userID'];?>;
const configSolution = <?php print json_encode($configSolution); ?>;
const solutionForm = new FormBuilder(configSolution, '#target', 'Add solution');
solutionForm.createTemplate2();

const configProblem = <?php print json_encode($configProblem); ?>;
const problemForm = new FormBuilder(configProblem, '#target', '');
problemForm.createTemplate();

getSolutions(<?php echo $row['id']; ?>);
/****************************/
$('#solutions-list').on('click', '.accept-problem', function(e){
    e.preventDefault();
    const $this = $(this);
    const id = $(this).data('id');
    const data ='id='+id+'&solution=1';
    $.ajax({
        url: 'register_rating.php?'+data,
        method: 'GET',
        success: function(str){
            getSolutions(<?php echo $row['id']; ?>);
            updatePointsNew();
        }
    });
});
$('#solutions-list').on('click', '.reject-problem', function(e){
    e.preventDefault();
    const $this = $(this);
    const id = $(this).data('id');
    const data ='id='+id+'&solution=0';
    $.ajax({
        url: 'register_rating.php?'+data,
        method: 'GET',
        success: function(str){
            getSolutions(<?php echo $row['id']; ?>);
            updatePointsNew();
        }
    });
});
$('#solutions-list').on('click', '.shortlist-problem', function(e){
    e.preventDefault();
    const id = $(this).data('id');
    $(this).toggleClass('active');
    if($('.active').length > <?php echo $maxSolutions; ?>){
        $(this).toggleClass('active');
        alert('Max <?php echo $maxSolutions; ?> solutions shortlisted');
    } else{
        if( $(this).hasClass('active') ) {
            s = 3;
            $(this).find('img').attr('src', 'images/emas/shortlist-ok.png');
            $(this).find('span').text('Shortlisted');
        }
        else{
            s = 2;
            $(this).find('img').attr('src', 'images/emas/shortlist-ko.png');
            $(this).find('span').text('Shortlist');
        }
        const data ='id='+id+'&solution=' + s;
        $.ajax({
            url: 'register_rating.php?'+data,
            method: 'GET',
            success: function(str){
                getSolutions(<?php echo $row['id']; ?>);
            }
        });
    }
});
/**/
let messages = ['scorer-message-1.png', 'scorer-message-2.png', 'scorer-message-3.png', 'scorer-message-4.png', 'scorer-message-5.png', 'scorer-message-6.png'];
var item = messages[Math.floor(Math.random()*messages.length)];
function updatePointsNew(){
        console.log('updatePoints');
        $.ajax({
            url: 'scorer.php?id=' + id,
            type: 'GET',
            dataType: 'json',
            success: function(data){
                }
        });
    }
</script>
<?php
get_footer();

