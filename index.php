<?php
/*
   +------------------------------------------------------------------------------+
   | License: EUPL 2                                                              |
   +------------------------------------------------------------------------------+
   | Authors: JRC Ispra Unit C.3 Living Labs for testing Digital Energy Solutions |
   | Date of last update: 01-07-2022                                              |
   |                                                                              |
   +------------------------------------------------------------------------------+
 */
header('location: welcome.php');
require('include/function.php');
get_header();
?>
<div class="container emas login" id="intro">
    <div class="row">
        <div class="col-md-12">
            <h1 class="welcome-title">Gamification<br>for environmental<br>sustainability</h1>
            <h4 class="welcome-text">A JRC Living Lab pilot project testing game-based<br>approaches to citizen engagement</h4>
        </div>
    </div>
    <div class="row">
        <div class="col-md-3"></div>
        <div class="col-md-6">
                <h1 style="text-align: center; margin: 60px 0">TEST PAGE</h1>
          </div>
        <div class="col-md-3"></div>
    </div>
</div>


<?php
get_footer();

