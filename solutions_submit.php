<?php

require('include/db.php');
require('include/function.php');

if(!$_SESSION['userID']){
    header('location: login.php');
    exit();
}

$_SESSION['pool'] = getPool($_SESSION['userID'], $db);
$catid = 0;
$catname='';
$action = filter_var($_POST['action'], FILTER_SANITIZE_STRING);
switch ($action){
  case 'insert':
    $title = ''; // filter_var($_POST['your-subject'], FILTER_SANITIZE_STRING);
    $cat_id = filter_var($_POST['cat_id'], FILTER_SANITIZE_NUMBER_INT);
    $cat_name = filter_var($_POST['cat_name'], FILTER_SANITIZE_STRING);
    $problem_id = filter_var($_POST['problem_id'], FILTER_SANITIZE_NUMBER_INT);
    $description_1 = filter_var($_POST['description_1'], FILTER_SANITIZE_STRING);
    $description_2 = filter_var($_POST['description_2'], FILTER_SANITIZE_STRING);
    $description_3 = filter_var($_POST['description_3'], FILTER_SANITIZE_STRING);
    if(strlen(trim($description_1)) > 2){
      $sql = 'INSERT INTO solutions (title, problem_id, description_1, description_2, description_3, user_id, cat_id, cat_name, pool) VALUES(:title, :problem_id, :description_1, :description_2, :description_3, :user_id, :cat_id, :cat_name, :pool)';
      $stmt = $db->prepare($sql);
      $stmt->bindParam(':user_id', $_SESSION['userID']);
      $stmt->bindParam(':title', $title);
      $stmt->bindParam(':problem_id', $problem_id);
      $stmt->bindParam(':description_1', $description_1);
      $stmt->bindParam(':description_2', $description_2);
      $stmt->bindParam(':description_3', $description_3);
      $stmt->bindParam(':cat_id', $cat_id);
      $stmt->bindParam(':cat_name', $cat_name);
      $stmt->bindParam(':pool', $_SESSION['pool']);
      $stmt->execute();
      require('config.php');
      require('lib/class_user.php');
      $user = new User($_SESSION['userID']);
      $user->addPoints($db, $pointsSolution, "submit_solution");
      echo 1;
    }
    else{
      echo 0;
    }
    break;

  case 'edit':
    $id = filter_var($_POST['id'], FILTER_SANITIZE_NUMBER_INT);
    $description_1 = filter_var($_POST['description_1'], FILTER_SANITIZE_STRING);
    $description_2 = filter_var($_POST['description_2'], FILTER_SANITIZE_STRING);
    $description_3 = filter_var($_POST['description_3'], FILTER_SANITIZE_STRING);
    if(strlen(trim($description_1)) > 5){
      $sql = 'UPDATE solutions SET description_1 = :description_1, description_2 = :description_2, description_3 = :description_3 WHERE id = :id LIMIT 1';
      $stmt = $db->prepare($sql);
      $stmt->bindParam(':id', $id);
      $stmt->bindParam(':description_1', $description_1);
      $stmt->bindParam(':description_2', $description_2);
      $stmt->bindParam(':description_3', $description_3);
      $stmt->execute();
    }
    echo 0;
    break;

  case 'delete':
    $id = filter_var($_POST['id'], FILTER_SANITIZE_NUMBER_INT);
    $sql = 'DELETE FROM solutions WHERE id = :id LIMIT 1';
    $stmt = $db->prepare($sql);
    $stmt->bindParam(':id', $id);
    $stmt->execute();
    require('config.php');
    require('lib/class_user.php');
    $user = new User($_SESSION['userID']);
    $user->addPoints($db, $pointsSolution, "submit_solution");
    echo 0;
    break;
  }
exit();
