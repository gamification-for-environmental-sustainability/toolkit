<?php
header("Set-Cookie: key=value; path=/; domain=jrc_gamification.test; HttpOnly; SameSite=Lax");
session_start();
function getPool($id, $db){
    $sql = 'SELECT pool FROM rel_user_category WHERE user_id = :id LIMIT 1 ';
    $stmt = $db->prepare($sql);
    $stmt->bindParam(':id', $id);
    $stmt->execute();
    $row = $stmt->fetch(PDO::FETCH_ASSOC);
    return($row['pool']);
}
function get_current_user_id(){
    return 1;
}
function get_header(){
    $str = '<!DOCTYPE html>';
    $str .= '<html class="no-js">';
    $str .= '<head>';
    $str .= '<meta charset="UTF-8">';
    $str .= '<meta name="viewport" content="width=device-width, initial-scale=1.0" >';
    $str .= '<link rel="stylesheet" href="css/bootstrap.min.css">';
    $str .= '<link rel="stylesheet" href="css/style.css">';
    $str .= '</head>';
    $str .= '<body></body>';
    $str .= '<header id="site-header" class="">';
    $str .= '</header>';
    echo $str;
}
function get_footer(){
    //$str = '<div class="container" style=" padding: 0;">';
    //$str .= '<div>';
//    $str .= '<div class="col-md-6 ll">A pilot project from the <strong>Living Lab <br>for testing Digital Energy Solutions</strong></div>';
//    $str .= '<div class="col-md-6 destra">';
//    $str .= '<img src="images/emas/ec-signature.png" alt="">';
//    $str .= '</div>';
    $str = '<br><div class="container" style=" padding: 0;"><img src="images/emas/gamification-footer.png" style="width: 100%;"></div>';
    //$str .= '</div>';
    //$str .= '</div>';
    $str .= '</body>';
    $str .= '</html>';
    echo $str;
}
