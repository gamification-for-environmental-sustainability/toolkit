<?php
require dirname(__FILE__). '/../config.php';
try {
    $db = new PDO("mysql:host=$hostname;dbname=$dbname", $user, $pass);
    $db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    $db->exec("set names utf8");
} catch (PDOException $e){
    echo 'Errore: '.$e->getMessage();
    die();
}