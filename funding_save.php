<?php
require('include/db.php');
require('include/function.php');

if(!$_SESSION['userID']){
    header('location: login.php');
    exit();
}
$id = $_SESSION['userID'];
if(isset($_POST['data'])) {
    $response = 'ko';
    $data = $_POST['data'];
    foreach ($data as $item) {
        $sql = "SELECT id FROM ideas_funding WHERE solution_id=:solution_id AND user_id=:user_id";
        $stmtIdeas = $db->prepare($sql);
        $stmtIdeas->bindParam(':solution_id', $item['id']);
        $stmtIdeas->bindParam(':user_id', $id);
        $stmtIdeas->execute();
        $totIdeas = $stmtIdeas->rowCount();
        if ($totIdeas == 0) {
            $sql = 'INSERT INTO ideas_funding(solution_id, user_id, funding) VALUES (:solution_id, :user_id, :funding)';
            $stmtIdea = $db->prepare($sql);
            $stmtIdea->bindParam(':solution_id', $item['id']);
            $stmtIdea->bindParam(':user_id', $id);
            $stmtIdea->bindParam(':funding', $item['val']);
            $stmtIdea->execute();
            $response = 'ok';
        }
        else{
            $sql = 'UPDATE ideas_funding SET funding = :funding WHERE solution_id = :solution_id LIMIT 1';
            $stmtIdea = $db->prepare($sql);
            $stmtIdea->bindParam(':solution_id', $item['id']);
            $stmtIdea->bindParam(':funding', $item['val']);
            $stmtIdea->execute();
            $response = 'ok';
        }
    }
    require('lib/class_user.php');
    $sql = 'SELECT id, user_id FROM solutions';
    $stmt = $db->prepare($sql);
    $stmt->execute();
    while ($row = $stmt->fetch(PDO::FETCH_ASSOC)) {
        $sql = "SELECT SUM(funding) AS tot FROM ideas_funding WHERE solution_id=" . $row['id'];
        $stmtIdeas = $db->prepare($sql);
        $stmtIdeas->execute();
        $rowIdeas = $stmtIdeas->fetch(PDO::FETCH_ASSOC);
        if ($rowIdeas['tot'] && $rowIdeas['tot'] > 0) {
            $sql = 'UPDATE solutions SET funding_tot = ' . $rowIdeas['tot'] . '  WHERE id=' . $row['id'] . ' LIMIT 1';
            $stmtx = $db->prepare($sql);
            $stmtx->execute();
        } else {
            $sql = 'UPDATE solutions SET funding_tot = 0  WHERE id=' . $row['id'] . ' LIMIT 1';
            $stmtx = $db->prepare($sql);
            $stmtx->execute();
        }
        $user = new User($row['user_id']);
        $user->addPoints($db, $pointsSolutionRated, "solution_token");
    }
    echo $response;
} else {
    echo 'Nodata';
}

