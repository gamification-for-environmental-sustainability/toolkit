<?php

require('include/db.php');
require('include/function.php');

if(!$_SESSION['userID']){
  header('location: login.php');
  exit();
}

$sql = 'SELECT * FROM category INNER JOIN rel_user_category ON rel_user_category.cat_id = category.id WHERE step = 1 AND user_id = :id';
$stmt = $db->prepare($sql);
$stmt->bindParam(':id', $_SESSION['userID']);
$stmt->execute();
$row = $stmt->fetch(PDO::FETCH_ASSOC);
$catid = $row['id'];
$catname = $row['name'];

require('config.php');

get_header();
$_SESSION['pool'] = getPool($_SESSION['userID'], $db);
?>
<div class="container emas problems" id="intro">
    <div class="row">
        <div class="menu-wrapper">
          <div class="menu-wrapper-2">
            <?php include('include/title.php'); ?>
          </div>
          <div class="menu-wrapper-3">
          </div>
        </div>
    </div>
    <div class="stage-wrapper">
    <?php include('include/score-wrapper.php'); ?>
    </div>
      <div class="row">
          <div class="col-md-6">
            <a href="problems_rating.php"><img src="images/emas/stage-1-progress.png"></a>
          </div>
      </div>
      <div class="row">
          <div class="col-md-12 wrapper-category">
            <div class="category-name category-name-<?php echo $row['id']; ?>"><?php echo $row['name']; ?></div>
            <div class="category-desc category-desc-<?php echo $row['id']; ?>"><?php echo $row['description']; ?></div>
          </div>
      </div>

      <div class="row problems-wrapper">
          <div class="col-md-12" style="padding: 0;">
                <div><h3 class="problems-list-title"></h3></div>
                <div id="problems-list"></div>
                <div class="problem-form problem-form-insert">
                    <form id="info">
                        <div id="target"></div>
                    </form>
                </div>
            </div>
        </div>
        <div class="row">
          <div class="col-md-12 text-center">
            <a href="problems_rating.php" class="next-stage">Go to next stage</a>
          </div>
        </div>
    </div>
</div>
<br><br>

<div class="single-problem template">
    <div class="title">
        <div class="title-int"></div>
    </div>
    <div class="heading"></div>
    <div class="problem-text"></div>
</div>

<div class="bonus-points">+50 points</div>
<script src="js/jquery-3.6.0.min.js"></script>
<script src="js/form.js"></script>
<script src="js/main.js"></script>
<script>
var id = <?php echo $_SESSION['userID'];?>;
const configProblem = <?php print json_encode($configProblem); ?>;
const problemForm = new FormBuilder(configProblem, '#target', 'Add this problem');
problemForm.createForm();
problemForm.createTemplate();

getProblems(0);
/* insert */
$('#info').on('click', '[type="submit"]', function(e){
    e.preventDefault();
    const dataForm = $('#info').serialize();
    const data = 'action=insert&' + dataForm;
    $.ajax({
        url: 'problems_submit.php',
        type: 'POST',
        data: data,
        success:function(r){
            getProblems(0);
            if(r == '1') {
              updatePoints();
              $('.bonus-points').show().fadeOut(3000);
            }
            $('#description_1').val('').trigger('keyup');
            $('#description_2').val('').trigger('keyup');
            $('#description_3').val('').trigger('keyup');
            $('#description_4 ').val('').trigger('keyup');
        }
    });
  });
  /* switch to form */
 $('body').on('click', '.single-problem', function(e){
    const id = $(this).find('.title').attr('id');
    let editForm = $('.problem-form-insert').clone().removeClass('problem-form-insert').css({'margin-bottom': '80px', 'position' : 'relative'});
    $(editForm)
        .append('<button type="button" class="delete-problem" data-id="' + id + '">Delete</button>')
        .find('#info').attr('id', 'editForm_' + id)
        .append('<input type="hidden" id="id" name="id" value="' + id + '">')
        .find('[type="submit"]').text('Save changes').addClass('edit-problem')
        .addClass('edit-form');

    $.ajax({
        url: 'problems_list_json.php?action=detail&id=' + id,
        type: 'GET',
        success:function(datasp){
            datasp = JSON.parse(datasp)
            $.each(configProblem, (i, el) => {
                let inputName;
                switch(el.type){
                    case 'textarea':
                        inputName = el.db;
                        $('#editForm_' + id +' #' + inputName).val( datasp[el.db] );
                        problemForm.checkChr(el.max, inputName, 'msg' + inputName);
                        break;
                    }
                });
        }
    });
    $(this).replaceWith(editForm);
 });
/* edit */
 $('#problems-list').on('click', '.edit-problem', function(e){
    e.preventDefault();
    const dataForm = $(this).parent().serialize();
    const data = 'action=edit&' + dataForm;
    $.ajax({
        url: 'problems_submit.php',
        type: 'POST',
        data: data,
        success:function(data){
          getProblems(0);
          updatePoints();
        }
    });
  });
/* delete */
 $('#problems-list').on('click', '.delete-problem', function(){
    const s = window.confirm('Are you sure?');
    if(s){
      const id = $(this).data('id');
      const data = 'action=delete&id=' + id;
      $.ajax({
          url: 'problems_submit.php',
          type: 'POST',
          data: data,
          success:function(data){
            getProblems(0);
            updatePoints();
          }
      });
    }
  });
</script>
<?php
get_footer();
