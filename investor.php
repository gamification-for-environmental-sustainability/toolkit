<?php

$catid = '';
$catname = '';

require('include/db.php');
require('include/function.php');

if(!$_SESSION['userID']){
    header('location: login.php');
    exit();
}

$id = 1;
if(isset($_GET['id'])){
    $id = filter_var($_GET['id'], FILTER_SANITIZE_NUMBER_INT);
}
if($id > 3){
    header('location: investor_post.php');
    exit();
}


$sql = 'SELECT * FROM category WHERE id = :id';
$stmt = $db->prepare($sql);
$stmt->bindParam(':id', $id, PDO::PARAM_INT);
$stmt->execute();
$row = $stmt->fetch(PDO::FETCH_ASSOC);

$catid = $row['id'];
$catname = $row['name'];

require('config.php');

$_SESSION['pool'] = getPool(get_current_user_id(), $db);
get_header();
?>
<div class="container emas investor" id="intro">
    <div class="row">
        <div class="menu-wrapper">
          <div class="menu-wrapper-2">
            <?php include('include/title.php'); ?>
          </div>
          <div class="menu-wrapper-3">

          </div>
        </div>
    </div>
    <div class="stage-wrapper">
    <?php include('include/score-wrapper.php'); ?>
    <div class="row">
        <div class="col-md-6">
            <img src="images/emas/stage-5-progress.png">
        </div>
      </div>

      <div class="row">
          <div class="col-md-12 wrapper-category">
            <div class="category-name category-name-<?php echo $row['id']; ?>"><?php echo $row['name']; ?></div>
            <div class="category-desc category-desc-<?php echo $row['id']; ?>"><?php echo $row['description']; ?></div>
          </div>
      </div>
      <div class="row">
          <div class="col-md-12" style="padding: 0;">
            <!--<div id="investor-title" class="investor-title">Funding solutions</div>-->
            <div id="investor-title" class="investor-title">Allocate up to 5 bags of gold per player</div>
        </div>
	</div>
      <div class="row">
          <div class="col-md-12" style="padding: 0;">
            <div id="investor" class="solutions-shortlisted-wrapper">
                <div id="solutions-list"></div>
                <div style="padding-top:20px; text-align: right; background-color: #FFFFFF;"><a class="submit-idea-button" href="#" style="width: 258px; font-size: 16px;">Confirm your investment</a></div>
            </div>
        </div>
	</div>
</div>
<div class="single-problem template">
    <div class="title">
        <div class="title-int"></div>
    </div>
    <div class="heading"></div>
    <div class="problem-text"></div>
</div>

<div class="single-problem single-solution template-2">
    <div class="button-solution">
        <div class="minus">-</div>
        <div class="value"> 0 </div>
        <div class="plus">+</div>
    </div>
    <div class="title">
        <div class="title-int"></div>
    </div>
    <div class="heading"></div>
    <div class="problem-text"></div>
</div>

<script src="js/jquery-3.6.0.min.js"></script>
<script src="js/form.js"></script>
<script src="js/main.js"></script>
<script>
var id = <?php echo $_SESSION['userID'];?>;

const configSolution = <?php print json_encode($configSolution); ?>;
const solutionForm = new FormBuilder(configSolution, '#target', 'Add solution');
solutionForm.createTemplate2();

const configProblem = <?php print json_encode($configProblem); ?>;
const problemForm = new FormBuilder(configProblem, '#target', '');
problemForm.createTemplate();

getSolutionsShortlisted(<?php echo $id; ?>);
/****************************/
/****************************/
$('#solutions-list').on('click', '.plus', function(e){
    let id = $(this).data('id');
    const solution = $('#solution' + id);
    let x = parseInt(solution.text());
    x++;
    solution.text(x);
});
$('#solutions-list').on('click', '.minus', function(e){
    let id = $(this).data('id');
    const solution = $('#solution' + id);
    let x = parseInt(solution.text());
    x--;
    if(x < 0) { x = 0; }
    solution.text(x);
});
/**/
$('.submit-idea-button').on('click', function(e){
  e.preventDefault();
  let v;
  let id;
  let a = [];
  const lista = $('.value');
  lista.each(function(i){
    v = parseInt($(this).text());
    if(v > 0){
      id = $(this).data('id');
      obj = { id: id, val: v };
      a.push(obj);
    }
  });

  $.ajax({
    url: 'funding_save.php',
    type: 'POST',
    data: {data: a},
    success: function(str){
         location.href = 'investor.php?id=<?php echo $id+1; ?>';
    }
  });
});
/**/
let messages = ['scorer-message-1.png', 'scorer-message-2.png', 'scorer-message-3.png', 'scorer-message-4.png', 'scorer-message-5.png', 'scorer-message-6.png'];
var item = messages[Math.floor(Math.random()*messages.length)];
function updatePointsNew(){
        $.ajax({
            url: 'scorer.php?id=' + id,
            type: 'GET',
            dataType: 'json',
            success: function(data){
                $('.point').text(data.points);
                $('#you_wrapper').rotate(data.gradi);

                var img1 = document.createElement('img');
                img1.src = "images/emas/scorer-happy-animation.gif?p" + new Date().getTime();

                var img2 = document.createElement('img');
                img2.src = "images/emas/scorer-neutral.png?p" + new Date().getTime();

                $('.stage-wrapper').addClass('animated');
                if($('.stage-wrapper').hasClass('animated')){
                    $(img1).on('load',function(){
                        $('#wrapper_circonferenza').css('background-image', 'url(' + img1.src + ')');
                    });
                    $('#circonferenza').append('<img src="images/emas/'+item+'" class="messages" id="msg1">');
                    $('#msg1').delay(2000).fadeOut(6000);
                    } else{
                        $(img2).on('load',function(){
                            $('#wrapper_circonferenza').css('background-image', 'url(' + img2.src + ')');
                        });
                    }
                }
        });
    }
</script>
<?php
get_footer();

