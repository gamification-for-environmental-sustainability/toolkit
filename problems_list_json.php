<?php

require('include/db.php');
require('include/function.php');

if(!$_SESSION['userID']){
    header('location: login.php');
    exit();
}

$_SESSION['pool'] = getPool($_SESSION['userID'], $db);
$id = filter_var($_GET['id'],FILTER_SANITIZE_NUMBER_INT);
if($id == 0) {
  $sql = 'SELECT * FROM problems WHERE user_id = :user_id ';
  $stmt = $db->prepare($sql);
  $stmt->bindParam(':user_id', $_SESSION['userID']);
  $stmt->execute();
  $rows =  $stmt->fetchAll(PDO::FETCH_ASSOC);
}
else{
    if(isset($_GET['action'])){
        $action = filter_var($_GET['action'],FILTER_SANITIZE_STRING);
    }
    else{
        $action = '';
    }
    switch($action){
        case 'accept':
            $current_userx = get_current_user_id();
            $sql = 'SELECT * FROM problems WHERE cat_id = :cat_id AND pool = :pool AND rating_ok = 1 ';
            $stmt = $db->prepare($sql);
            $stmt->bindParam(':cat_id', $id);
            $stmt->bindParam(':pool', $_SESSION['pool']);
            $stmt->execute();
            $rows =  $stmt->fetchAll(PDO::FETCH_ASSOC);
            break;
        case 'rejected':
            $current_userx = get_current_user_id();
            $sql = 'SELECT * FROM problems WHERE cat_id = :cat_id AND pool = :pool AND rating_ok = 0 ';
            $stmt = $db->prepare($sql);
            $stmt->bindParam(':cat_id', $id);
            $stmt->bindParam(':pool', $_SESSION['pool']);
            $stmt->execute();
            $rows =  $stmt->fetchAll(PDO::FETCH_ASSOC);
            break;
        case 'shortlisted':
            $current_userx = get_current_user_id();
            //$sql = 'SELECT * FROM solutions WHERE cat_id = :cat_id AND pool = :pool AND rating_shortlist = 1 ';
            //echo $sql;
            $sql = 'SELECT * FROM problems WHERE cat_id = :cat_id AND pool = :pool AND rating_shortlist = 1 ';
            $stmt = $db->prepare($sql);
            $stmt->bindParam(':cat_id', $id);
            $stmt->bindParam(':pool', $_SESSION['pool']);
            $stmt->execute();
            $rows =  $stmt->fetchAll(PDO::FETCH_ASSOC);
            break;
        case 'shortlisted_solutions':
            $rows = [];
            $current_userx = get_current_user_id();
            $sql = 'SELECT * FROM problems WHERE cat_id = :cat_id AND pool = :pool AND rating_shortlist = 1 ';
            $stmt = $db->prepare($sql);
            $stmt->bindParam(':cat_id', $id);
            $stmt->bindParam(':pool', $_SESSION['pool']);
            $stmt->execute();
            while($row =  $stmt->fetch(PDO::FETCH_ASSOC)){
                $sql = 'SELECT * FROM solutions WHERE problem_id = :problem_id ';
                $stmts = $db->prepare($sql);
                $stmts->bindParam(':problem_id', $row['id']);
                $stmts->execute();
                $rowsol = $stmts->fetchAll(PDO::FETCH_ASSOC);
                $row['solution'] = $rowsol;
                array_push($rows, $row);
            }
            break;
        case 'detail':
            $sql = 'SELECT * FROM problems WHERE id = :id ';
            $stmt = $db->prepare($sql);
            $stmt->bindParam(':id', $id);
            $stmt->execute();
            $rows =  $stmt->fetch(PDO::FETCH_ASSOC);
            break;
        default:
            $current_userx = get_current_user_id();
            $sql = 'SELECT * FROM problems WHERE cat_id = :cat_id AND pool = :pool AND rating_ok IS NULL ';
            $stmt = $db->prepare($sql);
            $stmt->bindParam(':cat_id', $id);
            $stmt->bindParam(':pool', $_SESSION['pool']);
            $stmt->execute();
            $rows =  $stmt->fetchAll(PDO::FETCH_ASSOC);
            break;
    }
}
print(json_encode($rows));

