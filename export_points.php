<?php
$catid=0;
$catname='';
require('include/db.php');
require('include/function.php');

if(!$_SESSION['userID']){
    header('location: login.php');
    exit();
}

get_header();
?>
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <table class="table">
                <thead>
                    <tr>
                        <th>User ID</th>
                        <!--<th>Team</th>-->
                        <th>Display Name</th>
                        <th>Total Points</th>
                        <th>Total Problems</th>
                        <th>Total Problems Points</th>
                        <th>Total Problems Approved</th>
                        <th>Total Problems Approved Points</th>
                        <th>Total Solutions</th>
                        <th>Total Solutions Points</th>
                        <th>Total Solutions Approved</th>
                        <th>Total Solutions Approved Points</th>
                        <th>Total Solutions Token</th>
                        <th>Total Solutions Token Points</th>
                    </tr>
                </thead>
                <tbody>
                <?php
                $sql = 'SELECT * FROM users_score WHERE total_points > 1 ORDER BY user_id';
                $stmt = $db->query($sql);
                while($row = $stmt->fetch(PDO::FETCH_ASSOC)){
                    /**Team*/
                    $sql = 'SELECT username, display_name FROM users WHERE id = :user_id ';
                    $stmt0 = $db->prepare($sql);
                    $stmt0->bindParam(':user_id', $row['user_id'], PDO::PARAM_INT);
                    $stmt0->execute();
                    $row0 = $stmt0->fetch(PDO::FETCH_ASSOC);
                    $username = $row0['username'];
                    $display_name = $row0['display_name'];
                    /**Tot problems*/
                    $sql = 'SELECT count(*) as tot FROM problems WHERE user_id = :user_id ';
                    $stmt0 = $db->prepare($sql);
                    $stmt0->bindParam(':user_id', $row['user_id'], PDO::PARAM_INT);
                    $stmt0->execute();
                    $row0 = $stmt0->fetch(PDO::FETCH_ASSOC);
                    $tot_problems = $row0['tot'];
                    /**Tot problems approved*/
                    $sql = 'SELECT count(*) as tot FROM problems WHERE rating_ok = 1 AND user_id = :user_id ';
                    $stmt0 = $db->prepare($sql);
                    $stmt0->bindParam(':user_id', $row['user_id'], PDO::PARAM_INT);
                    $stmt0->execute();
                    $row0 = $stmt0->fetch(PDO::FETCH_ASSOC);
                    $tot_problems_app = $row0['tot'];
                    /**Tot solutions*/
                    $sql = 'SELECT count(*) as tot FROM solutions WHERE user_id = :user_id ';
                    $stmt0 = $db->prepare($sql);
                    $stmt0->bindParam(':user_id', $row['user_id'], PDO::PARAM_INT);
                    $stmt0->execute();
                    $row0 = $stmt0->fetch(PDO::FETCH_ASSOC);
                    $tot_solutions = $row0['tot'];
                    /**Tot solutions approved*/
                    $sql = 'SELECT count(*) as tot FROM solutions WHERE rating_ok = 1 AND user_id = :user_id ';
                    $stmt0 = $db->prepare($sql);
                    $stmt0->bindParam(':user_id', $row['user_id'], PDO::PARAM_INT);
                    $stmt0->execute();
                    $row0 = $stmt0->fetch(PDO::FETCH_ASSOC);
                    $tot_solutions_app = $row0['tot'];
                    /**Tot solutions token*/
                    $sql = 'SELECT SUM(funding_tot) as tot FROM solutions WHERE user_id = :user_id ';
                    $stmt0 = $db->prepare($sql);
                    $stmt0->bindParam(':user_id', $row['user_id'], PDO::PARAM_INT);
                    $stmt0->execute();
                    $row0 = $stmt0->fetch(PDO::FETCH_ASSOC);
                    $tot_solutions_token = $row0['tot'];
                    if($tot_solutions_token == null) { $tot_solutions_token = 0; }
                    /**Tot problems points*/
                    $sql = 'SELECT points FROM users_points_gained WHERE description = "submit_problem" AND user_id = :user_id ';
                    $stmt0 = $db->prepare($sql);
                    $stmt0->bindParam(':user_id', $row['user_id'], PDO::PARAM_INT);
                    $stmt0->execute();
                    $row0 = $stmt0->fetch(PDO::FETCH_ASSOC);
                    $tot_problems_points = $row0['points'];
                    if($tot_problems_points == null) { $tot_problems_points = 0; }
                    /**Tot problems approved points*/
                    $sql = 'SELECT points FROM users_points_gained WHERE description = "approved_problem" AND user_id = :user_id ';
                    $stmt0 = $db->prepare($sql);
                    $stmt0->bindParam(':user_id', $row['user_id'], PDO::PARAM_INT);
                    $stmt0->execute();
                    $row0 = $stmt0->fetch(PDO::FETCH_ASSOC);
                    $tot_problems_app_points = $row0['points'];
                    if($tot_problems_app_points == null) { $tot_problems_app_points = 0; }
                    /**Tot solutions points*/
                    $sql = 'SELECT points FROM users_points_gained WHERE description = "submit_solution" AND user_id = :user_id ';
                    $stmt0 = $db->prepare($sql);
                    $stmt0->bindParam(':user_id', $row['user_id'], PDO::PARAM_INT);
                    $stmt0->execute();
                    $row0 = $stmt0->fetch(PDO::FETCH_ASSOC);
                    $tot_solutions_points = $row0['points'];
                    if($tot_solutions_points == null) { $tot_solutions_points = 0; }
                    /**Tot solutions approved points*/
                    $sql = 'SELECT points FROM users_points_gained WHERE description = "approved_solution" AND user_id = :user_id ';
                    $stmt0 = $db->prepare($sql);
                    $stmt0->bindParam(':user_id', $row['user_id'], PDO::PARAM_INT);
                    $stmt0->execute();
                    $row0 = $stmt0->fetch(PDO::FETCH_ASSOC);
                    $tot_solutions_app_points = $row0['points'];
                    if($tot_solutions_app_points == null) { $tot_solutions_app_points = 0; }
                    /**Tot solutions token points*/
                    $sql = 'SELECT points FROM users_points_gained WHERE description = "solution_token" AND user_id = :user_id ';
                    $stmt0 = $db->prepare($sql);
                    $stmt0->bindParam(':user_id', $row['user_id'], PDO::PARAM_INT);
                    $stmt0->execute();
                    $row0 = $stmt0->fetch(PDO::FETCH_ASSOC);
                    $tot_solutions_token_points = $row0['points'];
                    if($tot_solutions_token_points == null) { $tot_solutions_token_points = 0; }
                    /***/
                    echo '<tr>';
                    echo '<td>'.$row['user_id'].'</td>';
                    //echo '<td>'.$username.'</td>';
                    echo '<td>'.$display_name.'</td>';
                    echo '<td>'.$row['total_points'].'</td>';

                    echo '<td class="problems">'.$tot_problems.'</td>';
                    echo '<td>'.$tot_problems_points.'</td>';

                    echo '<td class="problems_app">'.$tot_problems_app.'</td>';
                    echo '<td>'.$tot_problems_app_points.'</td>';

                    echo '<td class="solutions">'.$tot_solutions.'</td>';
                    echo '<td>'.$tot_solutions_points.'</td>';

                    echo '<td class="solutions_app">'.$tot_solutions_app.'</td>';
                    echo '<td>'.$tot_solutions_app_points.'</td>';
                    
                    echo '<td class="token">'.$tot_solutions_token.'</td>';
                    echo '<td>'.$tot_solutions_token_points.'</td>';
                    
                    echo '</tr>';
                    }
                ?>
                </tbody>
                <tfoot>
                    <tr>
                        <th></th>
                        <th></th>
                        <th></th>
                        <th id="Total_Problems"></th>
                        <th></th>
                        <th id="Total_Problems_Approved"></th>
                        <th></th>
                        <th id="Total_Solutions"></th>
                        <th></th>
                        <th id="Total_Solutions_Approved"></th>
                        <th></th>
                        <th id="Total_Solutions_Token"></th>
                        <th></th>
                    </tr>
                </tfoot>
            </table>
        </div>
    </div>
</div>
<script src="js/jquery-3.6.0.min.js"></script>
<script>
    /****/
    let pt = 0;
    let list = $('.problems');
    list.each( (i, el) => {
        let p = parseInt($(el).text());
        pt += p;
    });
    $('#Total_Problems').text(pt);
    /****/
    pt = 0;
    list = $('.problems_app');
    list.each( (i, el) => {
        let p = parseInt($(el).text());
        pt += p;
    });
    $('#Total_Problems_Approved').text(pt);
    /****/
    pt = 0;
    list = $('.solutions');
    list.each( (i, el) => {
        let p = parseInt($(el).text());
        pt += p;
    });
    $('#Total_Solutions').text(pt);
    /****/
    pt = 0;
    list = $('.solutions_app');
    list.each( (i, el) => {
        let p = parseInt($(el).text());
        pt += p;
    });
    $('#Total_Solutions_Approved').text(pt);
    /****/
    pt = 0;
    list = $('.token');
    list.each( (i, el) => {
        let p = parseInt($(el).text());
        pt += p;
    });
    $('#Total_Solutions_Token').text(pt);
</script>
</body>
</html>
