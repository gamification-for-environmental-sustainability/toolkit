<?php

require('include/db.php');
require('include/function.php');

if(!$_SESSION['userID']){
    header('location: login.php');
    exit();
}

$_SESSION['pool'] = getPool($_SESSION['userID'], $db);

$id = filter_var($_GET['id'],FILTER_SANITIZE_NUMBER_INT);
if($id == 0) {
  $sql = 'SELECT * FROM solutions WHERE user_id = :user_id ';
  $stmt = $db->prepare($sql);
  $stmt->bindParam(':user_id', $_SESSION['userID']);
  $stmt->execute();
  $rows =  $stmt->fetchAll(PDO::FETCH_ASSOC);
}
else{
  if(isset($_GET['action'])){
    $action = filter_var($_GET['action'],FILTER_SANITIZE_STRING);
  }
  else{
    $action = '';
  }
  switch($action){
    /*
    case 'accept':
        $x = 0;
        $rows = [];
        $current_userx = get_current_user_id();
        $sql = 'SELECT * FROM problems WHERE cat_id = :cat_id AND pool = :pool AND rating_shortlist = 1 ';
        $stmt = $db->prepare($sql);
        $stmt->bindParam(':cat_id', $id);
        $stmt->bindParam(':pool', $_SESSION['pool']);
        $stmt->execute();
        while($row =  $stmt->fetch(PDO::FETCH_ASSOC)){
            $sql = 'SELECT * FROM solutions WHERE problem_id = :problem_id AND rating_ok = 1 ';
            $stmts = $db->prepare($sql);
            $stmts->bindParam(':problem_id', $row['id']);
            $stmts->execute();
            if($stmts->rowCount() > 0) {
                $rowsol = $stmts->fetchAll(PDO::FETCH_ASSOC);
                $row['solution'] = $rowsol;
                array_push($rows, $row);
            }
            else{
                unset($rows[$x]);
            }
            $x++;
        }
      break;
      */
      /*
    case 'rejected':
        $x = 0;
        $rows = [];
        $current_userx = get_current_user_id();
        $sql = 'SELECT * FROM problems WHERE cat_id = :cat_id AND pool = :pool AND rating_shortlist = 1 ';
        $stmt = $db->prepare($sql);
        $stmt->bindParam(':cat_id', $id);
        $stmt->bindParam(':pool', $_SESSION['pool']);
        $stmt->execute();
        while($row =  $stmt->fetch(PDO::FETCH_ASSOC)){
            $sql = 'SELECT * FROM solutions WHERE problem_id = :problem_id AND rating_ok = 0 ';
            $stmts = $db->prepare($sql);
            $stmts->bindParam(':problem_id', $row['id']);
            $stmts->execute();
            if($stmts->rowCount() > 0) {
                $rowsol = $stmts->fetchAll(PDO::FETCH_ASSOC);
                $row['solution'] = $rowsol;
                array_push($rows, $row);
            }
            else{
                unset($rows[$x]);
            }
            $x++;
        }
      break;
      */
    case 'shortlisted':
        $x = 0;
        $rows = [];
        $current_userx = get_current_user_id();
        // $sql = 'SELECT * FROM problems WHERE cat_id = :cat_id AND pool = :pool AND rating_shortlist = 1 ';
        $sql = 'SELECT * FROM problems WHERE cat_id = :cat_id AND rating_shortlist = 1 ';
        $stmt = $db->prepare($sql);
        $stmt->bindParam(':cat_id', $id);
        // $stmt->bindParam(':pool', $_SESSION['pool']);
        $stmt->execute();
        while($row =  $stmt->fetch(PDO::FETCH_ASSOC)){
          $sql = 'SELECT * FROM solutions WHERE problem_id = :problem_id AND rating_shortlist = 1  ';
          $stmts = $db->prepare($sql);
          $stmts->bindParam(':problem_id', $row['id']);
          $stmts->execute();
          if($stmts->rowCount() > 0) {
              $rowsol = $stmts->fetchAll(PDO::FETCH_ASSOC);
              $row['solution'] = $rowsol;
          }
          if($stmts->rowCount() == 0){
              unset($rows[$x]);
          }
          else{
            array_push($rows, $row);
          }
          $x++;
        }
  
      break;
    case 'detail':
        $sql = 'SELECT * FROM solutions WHERE id = :id ';
        $stmt = $db->prepare($sql);
        $stmt->bindParam(':id', $id);
        $stmt->execute();
        $rows =  $stmt->fetch(PDO::FETCH_ASSOC);
      break;
    default:
      $x = 0;
      $rows = [];
      $current_userx = get_current_user_id();
      $sql = 'SELECT * FROM problems WHERE cat_id = :cat_id AND pool = :pool AND rating_shortlist = 1 ';
      $stmt = $db->prepare($sql);
      $stmt->bindParam(':cat_id', $id);
      $stmt->bindParam(':pool', $_SESSION['pool']);
      $stmt->execute();
      while($row =  $stmt->fetch(PDO::FETCH_ASSOC)){
        $sql = 'SELECT * FROM solutions WHERE problem_id = :problem_id AND rating_ok IS NULL ';
        $stmtsn = $db->prepare($sql);
        $stmtsn->bindParam(':problem_id', $row['id']);
        $stmtsn->execute();
        if($stmtsn->rowCount() > 0) {
            $rowsol = $stmtsn->fetchAll(PDO::FETCH_ASSOC);
            $row['solutionn'] = $rowsol;
        }
        $sql = 'SELECT * FROM solutions WHERE problem_id = :problem_id AND rating_ok = 1 ';
        $stmtsa = $db->prepare($sql);
        $stmtsa->bindParam(':problem_id', $row['id']);
        $stmtsa->execute();
        if($stmtsa->rowCount() > 0) {
            $rowsol = $stmtsa->fetchAll(PDO::FETCH_ASSOC);
            $row['solutiona'] = $rowsol;
        }
        $sql = 'SELECT * FROM solutions WHERE problem_id = :problem_id AND rating_ok = 0 ';
        $stmtsr = $db->prepare($sql);
        $stmtsr->bindParam(':problem_id', $row['id']);
        $stmtsr->execute();
        if($stmtsr->rowCount() > 0) {
            $rowsol = $stmtsr->fetchAll(PDO::FETCH_ASSOC);
            $row['solutionr'] = $rowsol;
        }
        if($stmtsn->rowCount() == 0 && $stmtsa->rowCount() == 0 && $stmtsr->rowCount() == 0){
            unset($rows[$x]);
        }
        else{
          array_push($rows, $row);
        }
        $x++;
      }
      break;
  }
}
print(json_encode($rows));
