<?php
$catid = 0;
$catname = '';
require('../include/db.php');
/**********************************************DROP */
$sql = 'DROP TABLE IF EXISTS category;';
$db->query($sql);
$sql = 'DROP TABLE IF EXISTS problems;';
$db->query($sql);
$sql = 'DROP TABLE IF EXISTS ideas_funding;';
$db->query($sql);
$sql = 'DROP TABLE IF EXISTS rel_user_category;';
$db->query($sql);
$sql = 'DROP TABLE IF EXISTS solutions;';
$db->query($sql);
$sql = 'DROP TABLE IF EXISTS users_points_gained;';
$db->query($sql);
$sql = 'DROP TABLE IF EXISTS users_score;';
$db->query($sql);
$sql = 'DROP TABLE IF EXISTS users;';
$db->query($sql);
$sql = 'DROP VIEW IF EXISTS export_problems;';
$db->query($sql);
$sql = 'DROP VIEW IF EXISTS export_solutions;';
$db->query($sql);
/**********************************************category */
$sql = 'CREATE TABLE category (
    id tinyint(4) NOT NULL,
    name varchar(20) COLLATE utf8_unicode_ci NOT NULL,
    description varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL
  ) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;';
$db->query($sql);

$sql='ALTER TABLE category ADD PRIMARY KEY (id);';
$db->query($sql);

$sql='ALTER TABLE category MODIFY id int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1;';
$db->query($sql);

$sql='INSERT INTO category (id, name, description) VALUES
(1, "'.$topicName1.'",  "'.$topicDesc1.'"),
(2, "'.$topicName2.'",  "'.$topicDesc2.'"),
(3, "'.$topicName3.'",  "'.$topicDesc3.'");';
$db->query($sql);
/**********************************************problems */
$sql='CREATE TABLE problems (
  id int(11) NOT NULL,
  user_id int(11) DEFAULT NULL,
  cat_id tinyint(4) DEFAULT NULL,
  cat_name varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  title varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  description_1 text COLLATE utf8_unicode_ci DEFAULT NULL,
  description_2 text COLLATE utf8_unicode_ci DEFAULT NULL,
  description_3 text COLLATE utf8_unicode_ci DEFAULT NULL,
  description_4 text COLLATE utf8_unicode_ci DEFAULT NULL,
  rating_ok tinyint(4) DEFAULT NULL,
  rating_shortlist tinyint(4) DEFAULT NULL,
  pool tinyint(4) NOT NULL,
  data_insert timestamp NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;';
$db->query($sql);

$sql='ALTER TABLE problems ADD PRIMARY KEY (id);';
$db->query($sql);

$sql='ALTER TABLE problems MODIFY id int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1;';
$db->query($sql);
/**********************************************rel_user_category */
$sql='CREATE TABLE rel_user_category (
    relid tinyint(4) NOT NULL,
    user_id int(11) NOT NULL,
    cat_id tinyint(4) NOT NULL,
    step tinyint(4) NOT NULL,
    pool tinyint(4) NOT NULL
  ) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;';
  $db->query($sql);

$sql='ALTER TABLE rel_user_category ADD PRIMARY KEY (relid);';
$db->query($sql);

$sql='ALTER TABLE rel_user_category MODIFY relid tinyint(4) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1;';
$db->query($sql);

$sql='INSERT INTO rel_user_category (relid, user_id, cat_id, step, pool) VALUES
(1, 200, 1, 1, 1),
(2, 201, 2, 1, 1),
(3, 202, 3, 1, 1),
(4, 203, 1, 1, 2),
(5, 204, 1, 2, 2),
(6, 205, 3, 1, 2),
(7, 206, 1, 1, 3),
(8, 207, 2, 1, 3),
(9, 208, 3, 1, 3),
(10, 200, 3, 2, 1),
(11, 200, 2, 3, 1),
(12, 201, 1, 2, 1),
(13, 201, 3, 3, 1),
(14, 202, 2, 2, 1),
(15, 202, 1, 3, 1),
(16, 203, 3, 2, 2),
(17, 203, 2, 3, 2),
(18, 204, 3, 3, 2),
(19, 204, 2, 1, 2),
(20, 205, 2, 2, 2),
(21, 205, 1, 3, 2),
(22, 206, 3, 2, 3),
(23, 206, 2, 3, 3),
(24, 207, 1, 2, 3),
(25, 207, 3, 3, 3),
(26, 208, 2, 2, 3),
(27, 208, 1, 3, 3),
(28, 200, 1, 4, 1),
(29, 201, 2, 4, 1),
(30, 202, 3, 4, 1),
(31, 203, 1, 4, 2),
(32, 204, 2, 4, 2),
(33, 205, 3, 4, 2),
(34, 206, 1, 4, 3),
(35, 207, 2, 4, 3),
(36, 208, 3, 4, 3);';
$db->query($sql);

/**********************************************solutions */
$sql='CREATE TABLE solutions (
    id int(11) NOT NULL,
    user_id int(11) DEFAULT NULL,
    problem_id int(11) DEFAULT NULL,
    cat_id tinyint(4) DEFAULT NULL,
    cat_name varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
    title varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
    description_1 text COLLATE utf8_unicode_ci,
    description_2 text COLLATE utf8_unicode_ci,
    description_3 text COLLATE utf8_unicode_ci,
    rating_ok tinyint(1) DEFAULT NULL,
    rating_shortlist tinyint(1) DEFAULT NULL,
    funding_tot int(11) DEFAULT NULL,
    funding_avg decimal(10,2) DEFAULT NULL,
    funding_count int(11) DEFAULT NULL,
    pool tinyint(4) NOT NULL,
    data_insert timestamp NULL DEFAULT CURRENT_TIMESTAMP,
    data_updated datetime DEFAULT NULL
  ) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;';
$db->query($sql);

$sql='ALTER TABLE solutions ADD PRIMARY KEY (id);';
$db->query($sql);

$sql='ALTER TABLE solutions  MODIFY id int(11) NOT NULL AUTO_INCREMENT;';
$db->query($sql);

/**********************************************users */
$sql='CREATE TABLE users (
id int(11) NOT NULL,
  username varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  password varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  display_name varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  prefix varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  data_insert timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;';
$db->query($sql);

$sql='ALTER TABLE users ADD PRIMARY KEY (id);';
$db->query($sql);

$sql='ALTER TABLE users MODIFY id int(11) NOT NULL AUTO_INCREMENT;';
$db->query($sql);

$sql='INSERT INTO users (id, username, password, prefix, display_name) VALUES
(1, "admin", "$2y$10$C/pfH9WvCjbo7OQA2syWfuBbsPtJ3cfXVKI9yqMA/QTQITKoL6g1u", "", ""),
(200, "Team1A", "$2y$10$w83oNPm0zKereQ8OjHBG/.U.xz2BzK3n736/xdsrcK8hxZogx01ue", "1A", ""),
(201, "Team1B", "$2y$10$urZvTxzUaB.bUdRXzyS43e2yQyK96CHN/F7j2TEQViMs9bo3lz93.", "1B", ""),
(202, "Team1C", "$2y$10$FPkEFnVn84QY3zhUGZIo7OdkfuAoGz5/1TtIU6HjqJJEaNQzNtZge", "1C", ""),
(203, "Team2A", "$2y$10$uvhc.0P25PqZbSDQ3Tz.IeCK2nEMmmGrfOYxmL8jVNpaF6SCiiB5i", "2A", ""),
(204, "Team2B", "$2y$10$yuSQWIJJvYsCXGQI77L7XeHFsdPPtgPrZ.V7FCmG55FHgOOKo4klW", "2B", ""),
(205, "Team2C", "$2y$10$1gi78X8bOWAennfxzJvdEOMnUojKx90UnyHr77gx4ZdpUtDsX0mkK", "2C", ""),
(206, "Team3A", "$2y$10$/m0TNOmuuz6YP4V0keY/hu36.CHASkvW7cdsy0z.CVHiFFb7O8R9S", "3A", ""),
(207, "Team3B", "$2y$10$QR32nGO5Hv5J9RwvPgOllezCB1yT/ebyhgU9p7APTDaGzgqx5N2Im", "3B", ""),
(208, "Team3C", "$2y$10$WM6fyC3cPXpZdq5QsPnANuny04v9PUchHw4KoNScSJB0/BpBzSiiC", "3C", "");';
$db->query($sql);
/**********************************************users_points_gained */
$sql='CREATE TABLE users_points_gained (
    id int(11) NOT NULL,
    user_id int(11) NOT NULL,
    points int(11) NOT NULL DEFAULT "0",
    description varchar(50) COLLATE utf8_unicode_ci NOT NULL,
    data_updated timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
  ) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;';
$db->query($sql);

$sql='ALTER TABLE users_points_gained ADD PRIMARY KEY (id);';
$db->query($sql);

$sql='ALTER TABLE users_points_gained MODIFY id int(11) NOT NULL AUTO_INCREMENT;';
$db->query($sql);
/**********************************************users_score */
$sql='CREATE TABLE users_score (
    id int(11) NOT NULL,
    user_id int(11) DEFAULT "0",
    total_points int(11) DEFAULT "0",
    data_updated timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
  ) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;';
$db->query($sql);

$sql='ALTER TABLE users_score
    ADD PRIMARY KEY (id),
    ADD KEY user_id (user_id);';
$db->query($sql);

$sql='ALTER TABLE users_score MODIFY id int(11) NOT NULL AUTO_INCREMENT;';
$db->query($sql);

$sql='INSERT INTO users_score (id, user_id, total_points) VALUES (1, 1, 1);';
$db->query($sql);
/**********************************************ideas_funding */
$sql='CREATE TABLE ideas_funding (
id int(11) NOT NULL,
  solution_id int(11) NOT NULL,
  user_id int(11) NOT NULL,
  funding int(11) NOT NULL,
  created_at timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;';
$db->query($sql);

$sql='ALTER TABLE ideas_funding ADD PRIMARY KEY (id);';
$db->query($sql);

$sql='ALTER TABLE ideas_funding MODIFY id int(11) NOT NULL AUTO_INCREMENT;';
$db->query($sql);

/**********************************************VIEW export_problems */
$sql='CREATE VIEW export_problems  AS SELECT problems.id AS id, problems.pool AS pool, users.display_name AS display_name, problems.cat_name AS cat_name, problems.title AS title, problems.description_1 AS description_1, problems.description_2 AS description_2, problems.description_3 AS description_3, problems.description_4 AS description_4, problems.rating_ok AS rating_ok, problems.rating_shortlist AS rating_shortlist FROM (problems join users on((problems.user_id = users.id))) ORDER BY problems.pool ASC, users.display_name ASC ;';
$db->query($sql);

$sql='CREATE VIEW export_solutions AS SELECT solutions.pool AS pool, solutions.problem_id AS problem_id, problems.description_1 AS problem_title, users.display_name AS display_name, solutions.cat_name AS cat_name, solutions.title AS title, solutions.description_1 AS description_1, solutions.description_2 AS description_2, solutions.description_3 AS description_3, solutions.rating_ok AS rating_ok, solutions.rating_shortlist AS rating_shortlist, solutions.funding_tot AS funding_tot FROM (solutions join users on((solutions.user_id = users.id)) join problems on((solutions.problem_id = problems.id))) ORDER BY solutions.pool ASC, users.display_name ASC ;';
$db->query($sql);
