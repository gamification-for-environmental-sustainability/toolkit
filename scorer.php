<?php

$catid=0;
$catname='';
require('include/db.php');
require('include/function.php');

if(!$_SESSION['userID']){
    header('location: login.php');
    exit();
}

//get user points
$sql = 'SELECT * FROM users_score WHERE user_id=:id';
$stmt = $db->prepare($sql);
$stmt->bindParam(':id', $_SESSION['userID']);
$stmt->execute();
$user = $stmt->fetch(PDO::FETCH_ASSOC);
if($user['total_points'] == 0){ $user['total_points'] = 0; }

//get max points
$sql = 'SELECT MAX(total_points) as m FROM users_score ';
$stmt = $db->query($sql);
$max = $stmt->fetch(PDO::FETCH_ASSOC);

$max_points = $max['m'];
$your_points = $user['total_points'];
if($max_points < 1){ $max_points = 1;}
if($your_points < 1){ $your_points = 1;}

$deg = round((265 * $your_points)/$max_points);
if($deg < 20) {
  $deg = 20;
}
$array = array('points'=>$user['total_points'],'gradi'=>$deg);
print(json_encode($array));
