<?php
$catid = 0;
$catname = '';
require('include/db.php');
require('include/function.php');

$contenuto='';
$action='';if(isset($_REQUEST['action'])){$action=$_REQUEST['action'];}
switch($action){
    case 'login':
        $contenuto=login();
        break;
    case 'logout':
        $contenuto=logout();
        break;
}

function login() {
    global $db;
    $token = filter_var($_POST['CSRFToken'], FILTER_SANITIZE_STRING);
    if (!$token || $token !== $_SESSION['token']) {
        // return 405 http status code
        header($_SERVER['SERVER_PROTOCOL'] . ' 405 Method Not Allowed');
        exit;
    } else {
        $user=filter_var(trim($_POST['login_username']), FILTER_SANITIZE_STRING);
        $pwd=filter_var(trim($_POST['login_password']), FILTER_SANITIZE_STRING);

        $sql='select id, password, display_name from users where username = :user ';
        $stmt=$db->prepare($sql);
        $stmt->bindParam(':user', $user);
        $stmt->execute();
        if($stmt->rowCount()>0){
            $row=$stmt->fetch(PDO::FETCH_ASSOC);
            if(password_verify($pwd, $row['password'])){
                $_SESSION['userID']=$row['id'];
                $_SESSION['displayName']=$row['display_name'];
                header('location: index.php');
                exit();
            } else{
                $out='<div class="error">Username o password errate</div>';
                return($out);
            }
        }
        else{
            $out='<div class="error">Username o password errate</div>';
            return($out);
        }
    }
}
function logout(){
    session_start();
    unset($_SESSION['userID']);
    unset($_SESSION['displayName']);
    header('location: index.php');
    exit();
}
if(!isset($_SESSION['token'])) {
    $_SESSION['token'] = md5(uniqid(mt_rand(), true));
}
get_header();
?>
<div class="container emas login" id="intro">
    <div class="row">
        <div class="col-md-12">
            <h1 class="welcome-title">Gamification<br>for environmental<br>sustainability</h1>
            <h4 class="welcome-text">A JRC Living Lab pilot project testing game-based<br>approaches to citizen engagement</h4>
        </div>
    </div>
    <br><br><br><br><br><br>
    <div class="row form-login">
        <div class="col-md-4"></div>
        <div class="col-md-4">
            <h1 class="form-login-sign-in">Sign-in</h1>
            <h4 class="form-login-sign-in-text">Load your game and play to win points.</h4>
            <p><?php echo $contenuto; ?></p>
            <form name="loginform" id="loginform" action="?action=login" method="post">
                <input type="hidden" name="CSRFToken" value="<?php echo $_SESSION['token']; ?>">
                <p class="login-username">
                    <label for="user">Nome utente o indirizzo email</label>
                    <input type="text" name="login_username" id="user" class="input" value="" size="20">
                </p>
                <p class="login-password">
                    <label for="pass">Password</label>
                    <input type="password" name="login_password" id="pass" class="input" value="" size="20">
                </p>
                <p class="login-submit">
                    <input type="submit" name="wp-submit" id="wp-submit" class="button button-primary" value="Sign-in">
                </p>
            </form>
        </div>
        <div class="col-md-4"></div>
    </div>
</div>
<script src="js/jquery-3.6.0.min.js"></script>
<script src="js/main.js"></script>
<script>
var id = 0;
$('#intro').css({'background-image': 'url(images/emas/bg8.png)', 'bacground-repeat': 'no-repeat', 'background-size':'cover', 'background-position': '0'});
</script>
<?php
get_footer();

