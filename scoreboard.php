<?php

require('include/db.php');
require('include/function.php');
/*
if(!$_SESSION['userID']){
    header('location: login.php');
    exit();
}
*/
get_header();
?>
<div class="container emas scoreboard" id="intro">
    <div class="row">
        <div class="menu-wrapper">
          <div class="menu-wrapper-2">
            <?php include('include/title.php'); ?>
          </div>
        </div>
    </div>
    <div class="stage-wrapper">
      <div class="row">
        <div class="col-md-3"></div>
        <div class="col-md-6">
            <img src="images/emas/stage-6-progress.png">
        </div>
        <div class="col-md-3"></div>
      </div>
      <div class="row problems-wrapper" style="margin: 150px 0;">
          <div class="col-md-3"></div>
          <div class="col-md-6">
          <div class="winner_players_wrapper">
                <div class="winner_players_header">
                    <div class="winner_header_r winner_players">Rank</div>
                    <div class="winner_header_n winner_players">Group name</div>
                    <div class="winner_header_s winner_players">Total score</div>
                </div>
            <?php
            //$sql='SELECT * FROM users_score INNER JOIN users ON users_score.user_id = users.id WHERE total_points > 1 ORDER BY total_points DESC LIMIT 6';
            $sql='SELECT * FROM users_score INNER JOIN users ON users_score.user_id = users.id WHERE total_points > 1 ORDER BY total_points DESC '; // ECA
            $stmtp = $db->prepare($sql);
            $stmtp->execute();
            $x = 0;
            $y = 0;
            $oldp = 0;
            while($rowp = $stmtp->fetch(PDO::FETCH_ASSOC)){
                if(($y > 18) && ($oldp != $rowp['total_points'])){ break; }
                if($oldp != $rowp['total_points']){
                    $x++;
                    }
                if($oldp != $rowp['total_points']){
                    if($x == 1) { $p = 'st'; }
                    elseif($x == 2){ $p = 'nd';}
                    elseif($x == 3){ $p = 'rd';}
                    else{ $p = 'th';}
                    echo '<div class="winner_players_body winner_players_body'.$x.' wpb'.$y.'">';
                    echo '<div class="winner_players winner_players_body_r wpb'.$x.'">'.$x.'<sup style="font-size: 12px; font-weight: normal;">'.$p.'</sup></div>';
                    echo '<div class="winner_players winner_players_body_n wpb'.$x.'">'.$rowp['display_name'].'</div>';
                    echo '<div class="winner_players winner_players_body_s wpb'.$x.'">'.$rowp['total_points'].'</div>';
                    echo '</div>';
                }
                else{
                    if($oldp == $rowp['total_points']){ $strClass = 'winner_players_bodyx';}
                    else{ $strClass = '';}
                    echo '<div class="winner_players_body '.$strClass.' wpb'.$y.'">';
                    echo '<div class="winner_players winner_players_body_r wpb'.$x.'"></div>';
                    echo '<div class="winner_players winner_players_body_n wpb'.$x.'">'.$rowp['display_name'].'</div>';
                    echo '<div class="winner_players winner_players_body_s wpb'.$x.'">'.$rowp['total_points'].'</div>';
                    echo '</div>';
                }
                $y++;
                $oldp = $rowp['total_points'];
                }
            ?>
            </div>
        </div>
      </div>
    </div>
</div>

<script>

</script>
<script src="js/jquery-3.6.0.min.js"></script>
<script src="js/main.js"></script>
<script>
var id = <?php echo get_current_user_id() ;?>;
    $('#site-header').prepend('<div id="celebration"><img src="images/emas/confetti.png" style="max-width: 100%;"></div>');
    const list = $('.winner_players_body');
    const tot = list.length;
    let i = tot;
    setInterval(() => {
        $('.wpb' + i).fadeIn(1500).css('display', 'flex');
        i--;
    }, 2000);
</script>
<?php
get_footer();

