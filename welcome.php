<?php

$catid = 0;
$catname = '';
require('include/db.php');
require('include/function.php');

if(!$_SESSION['userID']){
    header('location: login.php');
    exit();
}

$mostra1 = 'hidden';
$mostra2 = '';

if($_SERVER['REQUEST_METHOD'] == 'POST'){
    $mostra1 = '';
    $mostra2 = 'hidden';

    $user_id = $_SESSION['userID'];
    $display_name = substr(filter_var($_POST['dn'], FILTER_SANITIZE_STRING),0,20);

    $sql = 'SELECT prefix FROM users WHERE id = :id LIMIT 1';
    $stmt = $db->prepare($sql);
    $stmt->bindParam(':id', $_SESSION['userID']);
    $stmt->execute();
    $row = $stmt->fetch(PDO::FETCH_ASSOC);
    $prefix = $row['prefix'];
    $display_name = $prefix .' - '.$display_name;

    $sql = 'UPDATE users SET display_name=:display_name WHERE id = :id LIMIT 1';
    $stmt = $db->prepare($sql);
    $stmt->bindParam(':id', $_SESSION['userID']);
    $stmt->bindParam(':display_name', $display_name);
    $stmt->execute();
    $_SESSION['displayName'] = $display_name;
}
$_SESSION['pool'] = getPool($_SESSION['userID'], $db);
get_header();
?>
<div class="container emas login" id="intro">
    <div class="row">
        <div class="col-md-12">
            <h1 class="welcome-title">Gamification<br>for environmental<br>sustainability</h1>
            <h4 class="welcome-text">A JRC Living Lab pilot project testing game-based<br>approaches to citizen engagement</h4>
        </div>
    </div>
    <br><br><br><br><br><br><br><br><br><br><br>
    <div class="row form-login">
        <div class="col-md-3"></div>
        <div class="col-md-6">
                <h1 class="form-login-sign-in" <?php echo $mostra1; ?>>POOL <?php echo $_SESSION['pool']; ?><br><span style="font-size: 36px;">Welcome, <?php echo $_SESSION['displayName']; ?><span></h1>
                <form <?php echo $mostra2; ?> action="" method="POST">
                    <input id="newname" type="text" name="dn" placeholder="Enter your Team Name (max 20 chars)" maxlength="20" style="width: 300px; margin-left: 160px; border-radius: 10px;"><br>
                    <p class="note form-idea-note" style="margin-top: 10px !important; text-align: center !important; color: #FFF;"><span id="form-idea-label-2-length">20</span> characters left</p>
                    <input type="submit" value="Save" class="form-login-link" style="margin-left: 236px; line-height: 18px;">
                </form>
                <div <?php echo $mostra1; ?> class="form-login-link"><a href="problems_list.php">Start your mission</a></div>
          </div>
        <div class="col-md-3"></div>
    </div>
</div>

<script src="js/jquery-3.6.0.min.js"></script>
<script src="js/main.js"></script>
<script>
var id = 0;
$('#intro').css({'background-image': 'url(images/emas/bg8.png)', 'bacground-repeat': 'no-repeat', 'background-size':'cover', 'background-position': '0'});
function checkChr(ctrl, out, len){
    let t2 = $('#'+ctrl).val().length;
    $('#'+out).text(len - t2);
    $('#'+ctrl).on('keyup', function(){
      let k = $(this).val();
      let t = $(this).val().length;
      $('#'+out).text(len - t);
      if(t > len){
        $(this).val(k.substr(0,len));
        $('#'+out).text(0);
      }
    });
}
checkChr('newname', 'form-idea-label-2-length', 20);
</script>
<?php
get_footer();

