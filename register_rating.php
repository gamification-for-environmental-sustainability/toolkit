<?php

require('include/db.php');
require('include/function.php');

if(!$_SESSION['userID']){
    header('location: login.php');
    exit();
}

$id = filter_var($_GET['id'], FILTER_SANITIZE_NUMBER_INT);
$solution = filter_var($_GET['solution'], FILTER_SANITIZE_NUMBER_INT);

$catid = 0;
$catname = '';
require('config.php');
require('lib/class_user.php');

if($solution > 1) {
    /**shortlist**/
    $solution = ($solution-2);
    $sql = 'UPDATE solutions SET rating_shortlist = :solution WHERE id = :id LIMIT 1';
    $stmt = $db->prepare($sql);
    $stmt->bindParam(':id', $id, PDO::PARAM_INT);
    $stmt->bindParam(':solution', $solution, PDO::PARAM_INT);
    $stmt->execute();
    $sql = 'SELECT user_id, cat_id FROM solutions WHERE id = :id LIMIT 1';
    $stmt = $db->prepare($sql);
    $stmt->bindParam(':id', $id, PDO::PARAM_INT);
    $stmt->execute();
    $row = $stmt->fetch(PDO::FETCH_ASSOC);

    $current_userx = $row['user_id'];
    $user = new User($current_userx);
    $user->addPoints($db, $pointsSolutionShortlisted, "shortlisted_solution", $row['cat_id']);
  }
else{


    $sql = 'SELECT rating_ok FROM solutions WHERE id = :id LIMIT 1';
    $stmt = $db->prepare($sql);
    $stmt->bindParam(':id', $id, PDO::PARAM_INT);
    $stmt->execute();
    $row = $stmt->fetch(PDO::FETCH_ASSOC);
    if( $row['rating_ok'] == NULL ){
      $x = true;
    }
    else{
      $x = false;
    }

    /**rating**/
    $sql = 'UPDATE solutions SET rating_ok = :solution, rating_shortlist = 0 WHERE id = :id LIMIT 1';
    $stmt = $db->prepare($sql);
    $stmt->bindParam(':id', $id, PDO::PARAM_INT);
    $stmt->bindParam(':solution', $solution, PDO::PARAM_INT);
    $stmt->execute();

    /**Points for approved solution**/
    $sql = 'SELECT user_id, cat_id FROM solutions WHERE id = :id LIMIT 1';
    $stmt = $db->prepare($sql);
    $stmt->bindParam(':id', $id, PDO::PARAM_INT);
    $stmt->execute();
    $row = $stmt->fetch(PDO::FETCH_ASSOC);

    $current_userx = $row['user_id'];
    $user = new User($current_userx);
    $user->addPoints($db, $pointsSolutionApproved, "approved_solution", $row['cat_id']);
  }
echo 0;
exit();
