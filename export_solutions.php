<?php
$catid = '';
$catname = '';

require('include/db.php');
require('include/function.php');
if(!$_SESSION['userID']){
    header('location: login.php');
    exit();
}

$header = "Pool;ProblemId;Problem;Team;Category;What;Where;Who;Approved;Shortlist;Funding;";
$data = '';
$sql = 'SELECT pool, problem_id, problem_title, display_name, cat_name, title, description_1, description_2, description_3, rating_ok, rating_shortlist, funding_tot FROM export_solutions WHERE 1';
$stmt = $db->query($sql);
while($row = $stmt->fetch(PDO::FETCH_ASSOC)){
    $data .= cleanString($row['pool']).";";
    $data .= cleanString($row['problem_id']).";";
    $data .= cleanString($row['problem_title']).";";
    $data .= cleanString($row['display_name']).";";
    $data .= cleanString($row['cat_name']).";";
    //$data .= cleanString($row['title']).";";
    $data .= cleanString($row['description_1']).";";
    $data .= cleanString($row['description_2']).";";
    $data .= cleanString($row['description_3']).";";
    $data .= cleanString($row['rating_ok']).";";
    $data .= cleanString($row['rating_shortlist']).";";
    $data .= cleanString($row['funding_tot']).";\n";
}
header("Content-type: application/vnd.ms-excel; name='excel'");
header("Content-Disposition: attachment; filename=exportfile.csv");
header("Pragma: no-cache");
header("Expires: 0");
// output data
echo $header."\n".$data;

function cleanString($s){
    $s = str_replace('&#34;','"',$s);
    $s = str_replace('&#39;','\'',$s);
    $s = str_replace(',','',$s);
    $s = str_replace(';','',$s);
    $s = trim(preg_replace('/\r|\n/', '', $s));
    return $s;
}
