<?php

require('include/db.php');
require('include/function.php');

if(!$_SESSION['userID']){
    header('location: login.php');
    exit();
}

$id = 1;
if(isset($_GET['id'])){
    $id = filter_var($_GET['id'], FILTER_SANITIZE_NUMBER_INT);
}
if($id > 3){
    header('location: investor_post.php');
    exit();
}
get_header();
?>
<div class="container emas investor" id="intro">
    <div class="row">
        <div class="menu-wrapper">
          <div class="menu-wrapper-2">
            <?php include('include/title.php'); ?>
          </div>
          <div class="menu-wrapper-3">

          </div>
        </div>
    </div>
    <div class="stage-wrapper">
    <?php include('include/score-wrapper.php'); ?>
    <div class="row">
        <div class="col-md-6">
            <img src="images/emas/stage-5-progress.png">
        </div>
      </div>
      <div class="row">
        <div class="col-md-2"></div>
        <div class="col-md-8 investor-post">
                <h1>Congratulations, <?php echo $_SESSION['displayName']; ?>!</h1>
                <p>
                    You have now successfully completed all stages of the "Gamification for environmental sustainability" created by the Living Lab for testing Digital Energy Solutions at JRC Ispra.<br>Thank you for being great team players! 
                </p>
          </div>
          <div class="col-md-2"></div>
      </div>
</div>
<script>
    var id = <?php echo $_SESSION['userID'];?>;
</script>
<script src="js/jquery-3.6.0.min.js"></script>
<script src="js/main.js"></script>
<script>
</script>
<?php
get_footer();

