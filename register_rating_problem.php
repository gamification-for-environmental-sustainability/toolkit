<?php

require('include/db.php');
require('include/function.php');

if(!$_SESSION['userID']){
    header('location: login.php');
    exit();
}

$catid = 0;
$catname = '';
require('config.php');
require('lib/class_user.php');

$id = filter_var($_GET['id'], FILTER_SANITIZE_NUMBER_INT);
$problem = filter_var($_GET['problem'], FILTER_SANITIZE_NUMBER_INT);

if($problem > 1) {
    /**shortlist**/
    $problem = ($problem-2);
    $sql = 'UPDATE problems SET rating_shortlist = :problem WHERE id = :id LIMIT 1';
    $stmt = $db->prepare($sql);
    $stmt->bindParam(':id', $id, PDO::PARAM_INT);
    $stmt->bindParam(':problem', $problem, PDO::PARAM_INT);
    $stmt->execute();

    /**Points for approved problems**/
    $sql = 'SELECT user_id, cat_id FROM problems WHERE id = :id LIMIT 1';
    $stmt = $db->prepare($sql);
    $stmt->bindParam(':id', $id, PDO::PARAM_INT);
    $stmt->execute();
    $row = $stmt->fetch(PDO::FETCH_ASSOC);

    $current_userx = $row['user_id'];
    $user = new User($current_userx);
    $user->addPoints($db, $pointsProblemShortlisted, "shortlisted_problem", $row['cat_id']);
}
else{
    $sql = 'SELECT rating_ok FROM problems WHERE id = :id LIMIT 1';
    $stmt = $db->prepare($sql);
    $stmt->bindParam(':id', $id, PDO::PARAM_INT);
    $stmt->execute();
    $row = $stmt->fetch(PDO::FETCH_ASSOC);
    if( $row['rating_ok'] == NULL ){
      $x = true;
    }
    else{
      $x = false;
    }

    /**rating**/
    $sql = 'UPDATE problems SET rating_ok = :problem, rating_shortlist = 0 WHERE id = :id LIMIT 1';
    $stmt = $db->prepare($sql);
    $stmt->bindParam(':id', $id, PDO::PARAM_INT);
    $stmt->bindParam(':problem', $problem, PDO::PARAM_INT);
    $stmt->execute();

    /**Points for approved problems**/
    $sql = 'SELECT user_id, cat_id FROM problems WHERE id = :id LIMIT 1';
    $stmt = $db->prepare($sql);
    $stmt->bindParam(':id', $id, PDO::PARAM_INT);
    $stmt->execute();
    $row = $stmt->fetch(PDO::FETCH_ASSOC);

    $current_userx = $row['user_id'];
    $user = new User($current_userx);
    $user->addPoints($db, $pointsProblemApproved, "approved_problem", $row['cat_id']);
  }
echo 0;
exit();
