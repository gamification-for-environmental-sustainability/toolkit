<?php

require('include/db.php');
require('include/function.php');

if(!$_SESSION['userID']){
    header('location: login.php');
    exit();
}

$sql = 'SELECT * FROM category INNER JOIN rel_user_category ON rel_user_category.cat_id=category.id WHERE step=3 AND user_id=:id';
$stmt = $db->prepare($sql);
$stmt->bindParam(':id', $_SESSION['userID']);
$stmt->execute();
$row = $stmt->fetch(PDO::FETCH_ASSOC);
$catid = $row['id'];
$catname = $row['name'];

require('config.php');

get_header();
$_SESSION['pool'] = getPool($_SESSION['userID'], $db);
?>
<div class="container emas solution" id="intro">
    <div class="row">
        <div class="menu-wrapper">
        <div class="menu-wrapper-2">
            <?php include('include/title.php'); ?>
          </div>
          <div class="menu-wrapper-3">
          </div>
        </div>
    </div>
    <div class="stage-wrapper">
    <?php include('include/score-wrapper.php'); ?>
      <div class="row">
        <div class="col-md-6">
            <a href="rating.php"><img src="images/emas/stage-3-progress.png"></a>
        </div>
      </div>
      <div class="row">
          <div class="col-md-12 wrapper-category">
            <div class="category-name category-name-<?php echo $row['id']; ?>"><?php echo $row['name']; ?></div>
            <div class="category-desc category-desc-<?php echo $row['id']; ?>"><?php echo $row['description']; ?></div>
          </div>
      </div>
      <div class="row problems-wrapper">
          <div class="col-md-12" style="padding: 0;">
            <div><h3 class="problems-list-title">Identified problems</h3></div>
            <div id="problems-list-shortlisted"></div>
            <div class="solutions-wrapper">
              <div><h3 class="solutions-list-title"></h3></div>
            </div>
          </div>
        </div>
        <div class="row">
          <div class="col-md-12 text-center">
            <a href="rating.php" class="next-stage">Go to next stage</a>
          </div>
        </div>
      </div>

<div class="single-problem template">
    <div class="title">
        <div class="title-int"></div>
    </div>
    <div class="heading"></div>
    <div class="problem-text"></div>
</div>

<div class="single-problem single-solution template-2">
    <div class="title">
        <div class="title-int"></div>
    </div>
    <div class="heading"></div>
    <div class="problem-text"></div>
</div>

<div class="problem-form problem-form-insert solution-form-insert">
    <form id="info">
        <div id="target"></div>
    </form>
</div>
<div class="bonus-points">+50 points</div>
</div>
<script src="js/jquery-3.6.0.min.js"></script>
<script src="js/form.js"></script>
<script src="js/main.js"></script>
<script>
var id = <?php echo $_SESSION['userID'];?>;
const configSolution = <?php print json_encode($configSolution); ?>;
const solutionForm = new FormBuilder(configSolution, '#target', 'Add solution');
solutionForm.createForm();
solutionForm.createTemplate2();

const configProblem = <?php print json_encode($configProblem); ?>;
const problemForm = new FormBuilder(configProblem, '#target', '');
problemForm.createTemplate();

getProblemsShortlistedSolutions(<?php echo $row['id']; ?>);
$('#problems-list-shortlisted').on('click', '.add-solution', function(){
  const form = $('.solution-form-insert').clone().removeClass('solution-form-insert').css({'margin-bottom': '60px', 'margin-left': '178px' });
  const id = $(this).prev().prev().prev().find('.title').attr('id');
  form.find('[type="submit"]').text('Add this solution');
  form.find("#problem_id").val(id);
  $(this).prev().css('height', 'auto');
  $(this).prev().append(form);
  $(this).hide();
  solutionForm.checkChr(250, 'description_1', 'msgdescription_1');
  solutionForm.checkChr(250, 'description_2', 'msgdescription_2');
  solutionForm.checkChr(250, 'description_3', 'msgdescription_3');
});

$('#problems-list-shortlisted').on('click', '[type="submit"]', function(e){
    e.preventDefault();
    const dataForm = $(this).parent().serialize();
    const data = 'action=insert&' + dataForm;
    $.ajax({
        url: 'solutions_submit.php',
        type: 'POST',
        data: data,
        success:function(r){
          console.log(r);
          if(r == '1') {
              $('.bonus-points').show().fadeOut(3000);
            }
          getProblemsShortlistedSolutions(<?php echo $row['id']; ?>, problem_id);
          updatePoints();
        }
    });
  });
  /***/
  $('#problems-list-shortlisted').on('click', '.edit-form', function(){
    const dataForm = $(this).parent().serialize();
    const data = 'action=edit&' + dataForm;
    $.ajax({
        url: 'solutions_submit.php',
        type: 'POST',
        data: data,
        success:function(data){
          getProblemsShortlistedSolutions(<?php echo $row['id']; ?>, problem_id);
          updatePoints();
        }
    });
  });
  /***/
  $('#problems-list-shortlisted').on('click', '.delete-problem', function(){
    const s = window.confirm('Are you sure?');
    if(s){
      const id = $(this).data('id');
      const data = 'action=delete&id=' + id;
      $.ajax({
          url: 'solutions_submit.php',
          type: 'POST',
          data: data,
          success:function(data){
            getProblemsShortlistedSolutions(<?php echo $row['id']; ?>, problem_id);
            updatePoints();
          }
      });
    }
  });
  /***/
  $('#problems-list-shortlisted').on('click', '.single-solution', function(){
    const id = $(this).find('.title').attr('id');
    console.log(id);

    let editForm = $('.problem-form-insert').clone().removeClass('problem-form-insert').css({'margin-bottom': '80px', 'margin-left': '178px', 'position' : 'relative'});
    $(editForm)
        .append('<button type="button" class="delete-problem" data-id="' + id + '">Delete</button>')
        .find('#info').attr('id', 'editForm_' + id)
        .append('<input type="hidden" id="id" name="id" value="' + id + '">')
        .find('[type="submit"]').text('Save changes').addClass('edit-problem').attr('type', 'button')
        .addClass('edit-form');

        $.ajax({
        url: 'solutions_list_json.php?action=detail&id=' + id,
        type: 'GET',
        success:function(datasp){
            datasp = JSON.parse(datasp)
            $.each(configProblem, (i, el) => {
                let inputName;
                switch(el.type){
                    case 'textarea':
                        inputName = el.db;
                        $('#editForm_' + id +' #' + inputName).val( datasp[el.db] );
                        problemForm.checkChr(el.max, inputName, 'msg' + inputName);
                        break;
                    }
                });
        }
    });

    $(this).replaceWith(editForm);
  });
  /***/

</script>
<?php
get_footer();

