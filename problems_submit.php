<?php

$catid = 0;
$catname = '';

require('include/db.php');
require('include/function.php');
require('config.php');

if(!$_SESSION['userID']){
    header('location: login.php');
    exit();
}

$_SESSION['pool'] = getPool($_SESSION['userID'], $db);

$action = filter_var($_POST['action'], FILTER_SANITIZE_STRING);
switch ($action){
  case 'insert':
    require('config.php');
    require('lib/class_user.php');

    $sql = 'SELECT id FROM problems WHERE user_id = :user_id';
    $stmtp = $db->prepare($sql);
    $stmtp->bindParam(':user_id', $_SESSION['userID']);
    $stmtp->execute();
    $tot = $stmtp->rowCount();
    $list = ['A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z'];

    $current_userx = get_current_user_id();
    $title = $list[$tot];
    $cat_id = filter_var($_POST['cat_id'], FILTER_SANITIZE_NUMBER_INT);
    $cat_name = filter_var($_POST['cat_name'], FILTER_SANITIZE_STRING);
    $description_1 = filter_var($_POST['description_1'], FILTER_SANITIZE_STRING);
    $description_2 = filter_var($_POST['description_2'], FILTER_SANITIZE_STRING);
    $description_3 = filter_var($_POST['description_3'], FILTER_SANITIZE_STRING);
    $description_4 = filter_var($_POST['description_4'], FILTER_SANITIZE_STRING);

    if(strlen(trim($description_1)) > 2){
      $sql = 'INSERT INTO problems (title, description_1, description_2, description_3, description_4, user_id, cat_id, cat_name, pool) VALUES(:title, :description_1, :description_2, :description_3, :description_4, :user_id, :cat_id, :cat_name, :pool)';
      $stmt = $db->prepare($sql);
      $stmt->bindParam(':user_id', $_SESSION['userID']);
      $stmt->bindParam(':title', $title);
      $stmt->bindParam(':description_1', $description_1);
      $stmt->bindParam(':description_2', $description_2);
      $stmt->bindParam(':description_3', $description_3);
      $stmt->bindParam(':description_4', $description_4);
      $stmt->bindParam(':cat_id', $cat_id);
      $stmt->bindParam(':cat_name', $cat_name);
      $stmt->bindParam(':pool', $_SESSION['pool']);
      $stmt->execute();

      $user = new User($_SESSION['userID']);
      $user->addPoints($db, $pointsProblem, "submit_problem");
      echo 1;
    }
    else{
      echo 0;
    }
    break;

  case 'edit':
    $id = filter_var($_POST['id'], FILTER_SANITIZE_NUMBER_INT);
    $description_1 = filter_var($_POST['description_1'], FILTER_SANITIZE_STRING);
    $description_2 = filter_var($_POST['description_2'], FILTER_SANITIZE_STRING);
    $description_3 = filter_var($_POST['description_3'], FILTER_SANITIZE_STRING);
    $description_4 = filter_var($_POST['description_4'], FILTER_SANITIZE_STRING);
    if(strlen(trim($description_1)) > 5){
      $sql = 'UPDATE problems SET description_1 = :description_1, description_2 = :description_2, description_3 = :description_3 , description_4 = :description_4 WHERE id = :id LIMIT 1';
      $stmt = $db->prepare($sql);
      $stmt->bindParam(':id', $id);
      $stmt->bindParam(':description_1', $description_1);
      $stmt->bindParam(':description_2', $description_2);
      $stmt->bindParam(':description_3', $description_3);
      $stmt->bindParam(':description_4', $description_4);
      $stmt->execute();
    }
    break;

  case 'delete':
    $id = filter_var($_POST['id'], FILTER_SANITIZE_NUMBER_INT);
    $sql = 'DELETE FROM problems WHERE id = :id LIMIT 1';
    $stmt = $db->prepare($sql);
    $stmt->bindParam(':id', $id);
    $stmt->execute();
    require('config.php');
    require('lib/class_user.php');
    $user = new User($_SESSION['userID']);
    $user->addPoints($db, $pointsProblem, "submit_problem");
    break;

  }
exit();
