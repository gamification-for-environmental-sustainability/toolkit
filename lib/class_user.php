<?php
class User{
    public $id;

    function __construct($id) {
      $this->id = $id;
    }
    public function addPoints($db, $points, $description, $cat_id=0) {
      switch($description){
        case 'submit_problem':
          $sql = 'SELECT id FROM problems WHERE user_id = :user_id';
          $stmtp = $db->prepare($sql);
          $stmtp->bindParam(':user_id', $this->id);
          $stmtp->execute();
          $tot = $stmtp->rowCount();
          break;
        case 'submit_solution':
          $sql = 'SELECT id FROM solutions WHERE user_id = :user_id';
          $stmtp = $db->prepare($sql);
          $stmtp->bindParam(':user_id', $this->id);
          $stmtp->execute();
          $tot = $stmtp->rowCount();
          break;
        case 'approved_problem':
          $sql = 'SELECT id FROM problems WHERE rating_ok = 1 AND user_id = :user_id';
          $stmtp = $db->prepare($sql);
          $stmtp->bindParam(':user_id', $this->id);
          $stmtp->execute();
          $tot = $stmtp->rowCount();
          break;
        case 'shortlisted_problem':
          $sql = 'SELECT id FROM problems WHERE rating_shortlist = 1 AND user_id = :user_id ';
          $stmtp = $db->prepare($sql);
          $stmtp->bindParam(':user_id', $this->id);
          $stmtp->execute();
          $tot = $stmtp->rowCount();
          break;
        case 'approved_solution':
          $sql = 'SELECT id FROM solutions WHERE rating_ok = 1 AND user_id = :user_id';
          $stmtp = $db->prepare($sql);
          $stmtp->bindParam(':user_id', $this->id);
          $stmtp->execute();
          $tot = $stmtp->rowCount();
          break;
        case 'shortlisted_solution':
          $sql = 'SELECT id FROM solutions WHERE rating_shortlist = 1 AND user_id = :user_id';
          $stmtp = $db->prepare($sql);
          $stmtp->bindParam(':user_id', $this->id);
          $stmtp->execute();
          $tot = $stmtp->rowCount();
          break;
        case 'solution_token':
          $sql = 'SELECT SUM(funding_tot) as tot FROM solutions WHERE user_id = :user_id';
          $stmtp = $db->prepare($sql);
          $stmtp->bindParam(':user_id', $this->id);
          $stmtp->execute();
          $rowp = $stmtp->fetch(PDO::FETCH_ASSOC);
          $tot = $rowp['tot'];
          break;
        default:
          $tot = 1;
          break;
        }

        /* */
        $tot = $tot * $points;
        $sql = 'SELECT id FROM users_points_gained WHERE user_id = :user_id AND description = :description ';
        $stmtp = $db->prepare($sql);
        $stmtp->bindParam(':user_id', $this->id);
        $stmtp->bindParam(':description', $description);
        $stmtp->execute();
        if($stmtp->rowCount() == 0 && $this->id > 0){
            $sql = 'INSERT INTO users_points_gained(user_id, points, description) VALUES(:user_id, '.$tot.', :description)';
            $stmtpi = $db->prepare($sql);
            $stmtpi->bindParam(':user_id', $this->id);
            $stmtpi->bindParam(':description', $description, PDO::PARAM_STR);
            $stmtpi->execute();
        }
        else{
            $sql = 'UPDATE users_points_gained SET points='.$tot.' WHERE user_id=:user_id AND description=:description LIMIT 1';
            $stmtpu = $db->prepare($sql);
            $stmtpu->bindParam(':user_id', $this->id);
            $stmtpu->bindParam(':description', $description, PDO::PARAM_STR);
            $stmtpu->execute();
        }
        $this->totalPoints($db);
      }
      private function totalPoints($db){
        $sql = 'SELECT SUM(points) as tot FROM users_points_gained WHERE user_id = :user_id ';
        $stmtx = $db->prepare($sql);
        $stmtx->bindParam(':user_id', $this->id);
        $stmtx->execute();
        $rowx = $stmtx->fetch(PDO::FETCH_ASSOC);

        $sql = 'SELECT id FROM users_score WHERE user_id = :user_id ';
        $stmtp = $db->prepare($sql);
        $stmtp->bindParam(':user_id', $this->id);
        $stmtp->execute();
        if($stmtp->rowCount() == 0 && $this->id > 0){
            $sql = 'INSERT INTO users_score(user_id) VALUES(:user_id)';
            $stmtpi = $db->prepare($sql);
            $stmtpi->bindParam(':user_id', $this->id);
            $stmtpi->execute();
        }

        $sql = 'UPDATE users_score set total_points=:points, data_updated=NOW() WHERE user_id = :user_id LIMIT 1';
        $stmti = $db->prepare($sql);
        $stmti->bindParam(':user_id', $this->id);
        $stmti->bindParam(':points', $rowx['tot']);
        $stmti->execute();
      }
}
