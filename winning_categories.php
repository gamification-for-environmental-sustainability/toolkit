<?php


$catid = 0;
$catname = '';

require('include/db.php');
require('include/function.php');

if(!$_SESSION['userID']){
    header('location: login.php');
    exit();
}
get_header();
?>
<div class="container emas scoreboard" id="intro">
    <div class="row">
        <div class="menu-wrapper">
          <div class="menu-wrapper-2">
            <?php include('include/title.php'); ?>
          </div>
        </div>
    </div>
    <div class="stage-wrapper">
      <div class="row">
        <div class="col-md-3"></div>
        <div class="col-md-6">
            <img src="images/emas/stage-6-progress.png">
        </div>
        <div class="col-md-3"></div>
      </div>
      <div class="row problems-wrapper">
          <div class="col-md-12">
            <h1 class="emas-winning-title">Categories</h1>
          </div>
      </div>
      <div class="row problems-wrapper">
          <div class="col-md-12">
            <h4 class="emas-winning-categories-title">Top Super Environmental Squad</h4>
            <p class="emas-winning-categories-text">Team with the highest<br>number of game points</p>
            <div class="emas-winning-categories-wrapper">
            <?php
            $sql = 'SELECT total_points FROM users_score ORDER BY total_points DESC LIMIT 1 ';
            $stmt = $db->prepare($sql);
            $stmt->execute();
            $row = $stmt->fetch(PDO::FETCH_ASSOC);
            $sql = 'SELECT display_name FROM users INNER JOIN users_score ON users.id = users_score.user_id WHERE total_points = '.$row['total_points'].' ';
            $stmt = $db->prepare($sql);
            $stmt->execute();
            while($row = $stmt->fetch(PDO::FETCH_ASSOC)){
                echo '<div class="emas-winning-categories-w">'.substr($row['display_name'],0,2).'<span style="display: block; font-size: 20px; line-height: 23px;">'.substr($row['display_name'],5).'</span></div>';
            }
            ?>
            </div>
          </div>
      </div>
      <div class="row problems-wrapper">
          <div class="col-md-6">
            <h4 class="emas-winning-categories-title">Issue Champions</h4>
            <p class="emas-winning-categories-text">Team who identified the most problems</p>
            <div class="emas-winning-categories-wrapper">
            <?php
            $sql = 'SELECT points FROM users_points_gained WHERE description = "submit_problem" ORDER BY points DESC LIMIT 1 ';
            $stmt = $db->prepare($sql);
            $stmt->execute();
            $row = $stmt->fetch(PDO::FETCH_ASSOC);
            $sql = 'SELECT username, display_name FROM users INNER JOIN users_points_gained ON users.id = users_points_gained.user_id WHERE description = "submit_problem" AND points = '.$row['points'].' ';
            $stmt = $db->prepare($sql);
            $stmt->execute();
            while($row = $stmt->fetch(PDO::FETCH_ASSOC)){
                echo '<div class="emas-winning-categories-ww">'.substr($row['display_name'],0,2).'<span style="display: block; font-size: 20px; line-height: 23px;">'.substr($row['display_name'],5).'</span></div>';
            }
            ?>
            </div>
          </div>
          <div class="col-md-6">
            <h4 class="emas-winning-categories-title">Ideator Award</h4>
            <p class="emas-winning-categories-text">Team who identified the most solutions</p>
            <div class="emas-winning-categories-wrapper">
            <?php
            $sql = 'SELECT points FROM users_points_gained WHERE description = "submit_solution" ORDER BY points DESC LIMIT 1 ';
            $stmt = $db->prepare($sql);
            $stmt->execute();
            $row = $stmt->fetch(PDO::FETCH_ASSOC);
            $sql = 'SELECT display_name FROM users INNER JOIN users_points_gained ON users.id = users_points_gained.user_id WHERE description = "submit_solution" AND points = '.$row['points'].' ';
            $stmt = $db->prepare($sql);
            $stmt->execute();
            while($row = $stmt->fetch(PDO::FETCH_ASSOC)){
                echo '<div class="emas-winning-categories-ww">'.substr($row['display_name'],0,2).'<span style="display: block; font-size: 20px; line-height: 23px;">'.substr($row['display_name'],5).'</span></div>';
            }
            ?>
            </div>
          </div>
      </div>
    </div>
</div>
<script>
var id = <?php echo get_current_user_id() ;?>;
</script>
<script src="js/jquery-3.6.0.min.js"></script>
<script src="js/main.js"></script>
<script>
$('#site-header').prepend('<div id="celebration"><img src="images/emas/confetti.png" style="max-width: 100%;"></div>');
</script>
<?php
get_footer();

