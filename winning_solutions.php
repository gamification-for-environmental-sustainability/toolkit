<?php


$catid = 0;
$catname = '';

require('include/db.php');
require('include/function.php');
/*
if(!$_SESSION['userID']){
    header('location: login.php');
    exit();
}
*/
get_header();
?>
<style>
    #confetti-canvas{ 
        position: absolute;
        max-width: 100%;
        overflow: hidden;
    }
</style>
<script src="js/confetti.js"></script>
<script>
    startConfetti();
</script>

<div class="container emas scoreboard" id="intro">
    <div class="row">
        <div class="menu-wrapper">
          <div class="menu-wrapper-2">
            <?php include('include/title.php'); ?>
          </div>
        </div>
    </div>
    <div class="stage-wrapper">
      <div class="row">
        <div class="col-md-3"></div>
        <div class="col-md-6">
            <img src="images/emas/stage-6-progress.png">
        </div>
        <div class="col-md-3"></div>
      </div>
      <div class="row problems-wrapper">
          <div class="col-md-12">
            <h1 class="emas-winning-title">Winning solutions</h1>
          </div>
      </div>
    <?php
        $sql = 'SELECT * FROM category  WHERE id = 1 ';
        $stmt = $db->prepare($sql);
        $stmt->execute();
        $row = $stmt->fetch(PDO::FETCH_ASSOC);
        ?>
    <div id="topic-1">
        <div class="row">
          <div class="col-md-12 wrapper-category">
            <div class="category-name category-name-<?php echo $row['id']; ?>" data-id="<?php echo $row['id']; ?>"><?php echo $row['name']; ?></div>
            <div class="category-desc category-desc-<?php echo $row['id']; ?>"><?php echo $row['description']; ?></div>
          </div>
      </div>
      <div class="row problems-wrapper">
          <div class="col-md-12">
            <div class="winner_players_wrapper">
            <?php
            $sql='SELECT funding_tot FROM solutions WHERE funding_tot > 0 ORDER BY funding_tot DESC LIMIT 1';
            $stmtp = $db->prepare($sql);
            $stmtp->execute();
            $rowp = $stmtp->fetch(PDO::FETCH_ASSOC);
            $funding_tot = $rowp['funding_tot'];
            //$sql='SELECT * FROM solutions INNER JOIN users ON solutions.user_id = users.id WHERE cat_id = 1 AND funding_tot > 0 ORDER BY funding_tot DESC LIMIT 3';
            $sql='SELECT * FROM solutions INNER JOIN users ON solutions.user_id = users.id WHERE cat_id = 1 AND rating_shortlist = 1 ORDER BY funding_tot DESC  ';
            $stmtp = $db->prepare($sql);
            $stmtp->execute();
            $x = 0;
            $y = 0;
            $oldp = 0;
            while($rowp = $stmtp->fetch(PDO::FETCH_ASSOC)){
                if($funding_tot == $rowp['funding_tot']){ $class = ''; } //red
                else{ $class = ''; }
                if(($y > 18) && ($oldp != $rowp['funding_tot'])){ break; }
                if($oldp != $rowp['funding_tot']){
                    $x++;
                    }
                if($oldp != $rowp['funding_tot']){
                    if($x == 1) { $p = 'Winning'; }
                    elseif($x == 2){ $p = '2nd';}
                    elseif($x == 3){ $p = '3rd';}
                    else{ $p = $x.'th';}

                    $hide = '';
                    if($x < 4){ $hide ='style="display: none;"'; }

                    echo '<div class="emas_winner_players_body cat_'.$row['id'].' winner_players_body'.$x.' position_'.$x.'" '.$hide.'>';
                    echo '<div class="winner_players emas_winner_players_body_r ewpb'.$x.' '.$class.'">'.$p.'<sup style="font-size: 12px; font-weight: normal;"></sup></div>';
                    echo '<div class="winner_players emas_winner_players_body_n ewpb'.$x.' '.$class.'">'.$rowp['description_1'].'</div>';
                    echo '<div class="winner_players emas_winner_players_body_s ewpb'.$x.'"><div class="wteam" style="font-size: 20px;">'.$rowp['display_name'].'</div></div>';
                    echo '</div>';
                }
                else{
                    if($oldp == $rowp['funding_tot']){ $strClass = 'winner_players_bodyx';}
                    else{ $strClass = '';}
                    echo '<div class="emas_winner_players_body '.$strClass.' cat_'.$row['id'].' position_'.$x.'" '.$hide.'>';
                    echo '<div class="winner_players emas_winner_players_body_r ewpb'.$x.' '.$class.'">'.$p.'</div>';
                    echo '<div class="winner_players emas_winner_players_body_n ewpb'.$x.' '.$class.'">'.$rowp['description_1'].'</div>';
                    echo '<div class="winner_players emas_winner_players_body_s ewpb'.$x.'"><div class="wteam" style="font-size: 20px;">'.$rowp['display_name'].'</div></div>';
                    echo '</div>';
                }
                $y++;
                $oldp = $rowp['funding_tot'];
                }
            ?>
            </div>
        </div>
      </div>
        <div style="padding-top:20px; background-color: #FFFFFF;">
            <a class="submit-idea-button" href="#" data-id="#topic-2" style="width: 158px; font-size: 16px; display: block; margin: auto;">Go To Next Topic</a>
        </div>
    </div>
      <!--------------------------------------------------------->
      <?php
        $sql = 'SELECT * FROM category  WHERE id = 2 ';
        $stmt = $db->prepare($sql);
        $stmt->execute();
        $row = $stmt->fetch(PDO::FETCH_ASSOC);
        ?>
    <div id="topic-2">
        <div class="row">
            <div class="col-md-12 wrapper-category">
                <div class="category-name category-name-<?php echo $row['id']; ?>" data-id="<?php echo $row['id']; ?>"><?php echo $row['name']; ?></div>
                <div class="category-desc category-desc-<?php echo $row['id']; ?>"><?php echo $row['description']; ?></div>
            </div>
        </div>
        <div class="row problems-wrapper">
            <div class="col-md-12">
                <div class="winner_players_wrapper">
                <?php
                //$sql='SELECT * FROM solutions INNER JOIN users ON solutions.user_id = users.id WHERE cat_id = 2 AND funding_tot > 0 ORDER BY funding_tot DESC LIMIT 3';
                $sql='SELECT * FROM solutions INNER JOIN users ON solutions.user_id = users.id WHERE cat_id = 2 AND rating_shortlist = 1 ORDER BY funding_tot DESC  ';
                $stmtp = $db->prepare($sql);
                $stmtp->execute();
                $x = 0;
                $y = 0;
                $oldp = 0;
                while($rowp = $stmtp->fetch(PDO::FETCH_ASSOC)){
                if($funding_tot == $rowp['funding_tot']){ $class = ''; } // red
                else{ $class = ''; }
                if(($y > 18) && ($oldp != $rowp['funding_tot'])){ break; }
                    if($oldp != $rowp['funding_tot']){
                        $x++;
                        }
                    if($oldp != $rowp['funding_tot']){
                        if($x == 1) { $p = 'Winning'; }
                        elseif($x == 2){ $p = '2nd';}
                        elseif($x == 3){ $p = '3rd';}
                        else{ $p = $x.'th';}

                        $hide = '';
                        if($x < 4){ $hide ='style="display: none;"'; }

                        echo '<div class="emas_winner_players_body cat_'.$row['id'].' position_'.$x.' winner_players_body'.$x.'" '.$hide.'>';
                        echo '<div class="winner_players emas_winner_players_body_r ewpb'.$x.' '.$class.'">'.$p.'<sup style="font-size: 12px; font-weight: normal;"></sup></div>';
                        echo '<div class="winner_players emas_winner_players_body_n ewpb'.$x.' '.$class.'">'.$rowp['description_1'].'</div>';
                        echo '<div class="winner_players emas_winner_players_body_s ewpb'.$x.'"><div class="wteam" style="font-size: 20px;">'.$rowp['display_name'].'</div></div>';
                        echo '</div>';
                    }
                    else{
                        if($oldp == $rowp['funding_tot']){ $strClass = 'winner_players_bodyx';}
                        else{ $strClass = '';}
                        echo '<div class="emas_winner_players_body '.$strClass.' cat_'.$row['id'].' position_'.$x.'" '.$hide.'>';
                        echo '<div class="winner_players emas_winner_players_body_r ewpb'.$x.' '.$class.'">'.$p.'</div>';
                        echo '<div class="winner_players emas_winner_players_body_n ewpb'.$x.' '.$class.'">'.$rowp['description_1'].'</div>';
                        echo '<div class="winner_players emas_winner_players_body_s ewpb'.$x.'"><div class="wteam" style="font-size: 20px;">'.$rowp['display_name'].'</div></div>';
                        echo '</div>';
                    }
                    $y++;
                    $oldp = $rowp['funding_tot'];
                    }
                ?>
                </div>
            </div>
        </div>
        <div style="padding-top:20px; background-color: #FFFFFF;">
            <a class="submit-idea-button" href="#" data-id="#topic-3" style="width: 158px; font-size: 16px; display: block; margin: auto;">Go To Next Topic</a>
        </div>
      </div>
      <!--------------------------------------------------------->
      <?php
        $sql = 'SELECT * FROM category  WHERE id = 3 ';
        $stmt = $db->prepare($sql);
        $stmt->execute();
        $row = $stmt->fetch(PDO::FETCH_ASSOC);
        ?>
      <div id="topic-3">
        <div class="row">
            <div class="col-md-12 wrapper-category">
                <div class="category-name category-name-<?php echo $row['id']; ?>" data-id="<?php echo $row['id']; ?>"><?php echo $row['name']; ?></div>
                <div class="category-desc category-desc-<?php echo $row['id']; ?>"><?php echo $row['description']; ?></div>
            </div>
        </div>
        <div class="row problems-wrapper">
            <div class="col-md-12">
                <div class="winner_players_wrapper">
                <?php
                //$sql='SELECT * FROM solutions INNER JOIN users ON solutions.user_id = users.id WHERE cat_id = 3 AND funding_tot > 0 ORDER BY funding_tot DESC LIMIT 3';
                $sql='SELECT * FROM solutions INNER JOIN users ON solutions.user_id = users.id WHERE cat_id = 3 AND rating_shortlist = 1 ORDER BY funding_tot DESC  ';
                $stmtp = $db->prepare($sql);
                $stmtp->execute();
                $x = 0;
                $y = 0;
                $oldp = 0;
                while($rowp = $stmtp->fetch(PDO::FETCH_ASSOC)){
                if($funding_tot == $rowp['funding_tot']){ $class = ''; } // red
                else{ $class = ''; }
                if(($y > 18) && ($oldp != $rowp['funding_tot'])){ break; }
                    if($oldp != $rowp['funding_tot']){
                        $x++;
                        }
                    if($oldp != $rowp['funding_tot']){
                        if($x == 1) { $p = 'Winning'; }
                        elseif($x == 2){ $p = '2nd';}
                        elseif($x == 3){ $p = '3rd';}
                        else{ $p = $x.'th';}

                        $hide = '';
                        if($x < 4){ $hide ='style="display: none;"'; }

                        echo '<div class="emas_winner_players_body cat_'.$row['id'].' position_'.$x.' winner_players_body'.$x.'" '.$hide.'>';
                        echo '<div class="winner_players emas_winner_players_body_r ewpb'.$x.' '.$class.'">'.$p.'<sup style="font-size: 12px; font-weight: normal;"></sup></div>';
                        echo '<div class="winner_players emas_winner_players_body_n ewpb'.$x.' '.$class.'">'.$rowp['description_1'].'</div>';
                        echo '<div class="winner_players emas_winner_players_body_s ewpb'.$x.'"><div class="wteam" style="font-size: 20px;">'.$rowp['display_name'].'</div></div>';
                        echo '</div>';
                    }
                    else{
                        if($oldp == $rowp['funding_tot']){ $strClass = 'winner_players_bodyx';}
                        else{ $strClass = '';}
                        echo '<div class="emas_winner_players_body cat_'.$row['id'].' position_'.$x.' '.$strClass.'" '.$hide.'>';
                        echo '<div class="winner_players emas_winner_players_body_r ewpb'.$x.' '.$class.'">'.$p.'</div>';
                        echo '<div class="winner_players emas_winner_players_body_n ewpb'.$x.' '.$class.'">'.$rowp['description_1'].'</div>';
                        echo '<div class="winner_players emas_winner_players_body_s ewpb'.$x.'"><div class="wteam" style="font-size: 20px;">'.$rowp['display_name'].'</div></div>';
                        echo '</div>';
                    }
                    $y++;
                    $oldp = $rowp['funding_tot'];
                    }
                ?>
                </div>
            </div>
        </div>
        </div>
    </div>
</div>
<style>
    .position_1 .emas_winner_players_body_n{background: linear-gradient(90deg, rgba(178,157,225,1) 0%, rgba(255,255,255,1) 100%); border-radius: 10px; padding: 10px;}
    .position_2 .emas_winner_players_body_n{background: linear-gradient(90deg, rgba(207,195,236,1) 0%, rgba(255,255,255,1) 100%); border-radius: 10px; padding: 10px;}
    .position_3 .emas_winner_players_body_n{background: linear-gradient(90deg, rgba(233,227,246,1) 0%, rgba(255,255,255,1) 100%); border-radius: 10px; padding: 10px;}
</style>
<script>
var id = <?php echo get_current_user_id() ;?>;
</script>
<script src="js/jquery-3.6.0.min.js"></script>
<script src="js/main.js"></script>
<script src="js/confetti.js"></script>
<script>
    let c = 3;
    startConfetti();
//    $('#site-header').prepend('<div id="celebration"><img src="images/emas/confetti.png" style="max-width: 100%;"></div>');
    $('.submit-idea-button').on('click', function() {
        const h = $(this).data('id');
        console.log(h);
        if(h == '#topic-2' ){
            $('#topic-1').fadeOut();
            $('#topic-2').fadeIn();
            window.scrollTo(0, 0);
            c = 3;
        }
        if(h == '#topic-3' ){
            $('#topic-2').fadeOut();
            $('#topic-3').fadeIn();
            window.scrollTo(0, 0);
            c = 3;
        }
    });
    $('.category-name').on('click', function(){
        const id = $(this).data('id');
        $('.cat_' + id + '.position_' + c).fadeIn(2000);
        console.log(c);
        c--;
    });
</script>
<?php
get_footer();

